## 2021-04-09 -- v1.0.11
* Init Sentry on startup.

## 2021-04-08 -- v1.0.10
* Show accounts tab for production env.

## 2021-04-08 -- v1.0.9
* StopPropagation for "x" button on accounts list.

## 2021-04-07 -- v1.0.8
* Truncate pool tokens decimals the same way they are truncated in pool data table in Dexter.
* Sort tokens by xtzPool value.

## 2021-04-06 -- v1.0.7
* Use bs-css-emotion version 2.4.0

### 2020-04-06 -- v1.0.6
* Change the stats format so only currencies with value above 100 get rounded.
* Hide accounts tab in stag/prod.

### 2020-04-01 -- v1.0.5
* Rename sentry_dns to sentry_dsn

### 2020-03-30 -- v1.0.4
* Change number of decimals displayed to be max 6.
* Change empty positions message on account page.

### 2020-03-26 -- v1.0.3
* Fix lqtPercentage calculation.

### 2020-03-26 -- v1.0.2
* Add Liquidity Provided stat to account page.
* Add Liquidity Rewards stat to account page.
* Add Transactions stat to account page.
* Use front-end cache for CoinGecko.

### 2020-03-15 -- v1.0.1
* Add version number in UI.
* Handle new format of response from /transactions/current endpoint.

### 2020-12-17 -- v1.0.0
* Prepare phase 1 for launch.

### 2020-11-12 -- v0.1.0
* Init the caldera-frontend project.
