open Jest;
open Expect;
open ReactTestingLibrary;

describe("Snapshots", () => {
  test("Renders", () => {
    render(<Layout />) |> container |> expect |> toMatchSnapshot
  })
});
