open Jest;
open Expect;

describe("addBrackets", () => {
  test("0.1, (0.1)", () => {
    expect("0.1" |> Format.addBrackets) |> toBe("(0.1)")
  });
  test("3.24, (3.24)", () => {
    expect("3.24" |> Format.addBrackets) |> toBe("(3.24)")
  });
  test("test, (test)", () => {
    expect("test" |> Format.addBrackets) |> toBe("(test)")
  });
});

describe("truncateDecimals", () => {
  test("3, 0., 0", () => {
    expect(0. |> Format.truncateDecimals(3)) |> toBe("0")
  });
  test("3, 0.012345, 0.012", () => {
    expect(0.012345 |> Format.truncateDecimals(3)) |> toBe("0.012")
  });
  test("3, 0.098765, 0.099", () => {
    expect(0.098765 |> Format.truncateDecimals(3)) |> toBe("0.099")
  });
  test("3, 0.001234, 0.001", () => {
    expect(0.001234 |> Format.truncateDecimals(3)) |> toBe("0.001")
  });
  test("3, 0.0009, <0.001", () => {
    expect(0.0009 |> Format.truncateDecimals(3)) |> toBe("<0.001")
  });
});
