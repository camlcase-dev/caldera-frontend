module App = {
  [@react.component]
  let make = () => {
    let {path, search, hash} = ReasonReactRouter.useUrl();
    let url = "/" ++ (path |> Array.of_list |> Js.Array.joinWith("/"));

    React.useEffect3(
      () => {
        Analytics_Amplitude.logRouteVisit(
          url ++ "?" ++ search ++ "#" ++ hash,
        );
        None;
      },
      (url, search, hash),
    );

    Utils.isMaintenanceMode()
      ? <MaintenanceMode />
      : <ResponsiveContext.Provider>
          <CacheContext.Provider>
            <ContractsContext.Provider>
              <CurrenciesContext.Provider>
                <AccountsContext.Provider>
                  <MessageContext.Provider>
                    <Layout />
                  </MessageContext.Provider>
                </AccountsContext.Provider>
              </CurrenciesContext.Provider>
            </ContractsContext.Provider>
          </CacheContext.Provider>
        </ResponsiveContext.Provider>;
  };
};

ReactDOMRe.renderToElementWithId(<App />, "root");
