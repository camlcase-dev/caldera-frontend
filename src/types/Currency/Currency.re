type t = {
  decimals: int,
  name: string,
  prefixed: bool,
  rate: float,
  sign: option(string),
  symbol: string,
};

let default = "usd";

let popular: array(string) = [|
  "usd",
  "xtz",
  "eur",
  "gbp",
  "jpy",
  "rub",
  "btc",
  "eth",
|];

let getPopular = (currencies: list(t)): list(t) =>
  currencies
  |> List.filter((currency: t) =>
       popular |> Js.Array.includes(currency.symbol)
     );

let getOther = (currencies: list(t)): list(t) =>
  currencies
  |> List.filter((currency: t) =>
       !(popular |> Js.Array.includes(currency.symbol))
     );

let filterByText = (text: string, currencies: list(t)): list(t) => {
  let text = text |> Js.String.toLowerCase;

  text === ""
    ? currencies
    : currencies
      |> List.filter(currency => {
           currency.symbol
           |> Js.String.toLowerCase
           |> Js.String.includes(text)
           || currency.name
           |> Js.String.toLowerCase
           |> Js.String.includes(text)
         });
};

let isXTZ = (t: t): bool => t.symbol === "xtz";

let addCurrencySymbol = (currency: t, value: string): string => {
  let symbol =
    currency.sign
    |> Utils.orDefault(" " ++ currency.symbol |> Js.String.toUpperCase);

  (currency.prefixed ? symbol : "")
  ++ value
  ++ (!currency.prefixed ? symbol : "");
};

let toStringWithCommas =
    (currency: option(t), value: option(float)): option(string) =>
  switch (currency, value) {
  | (Some(currency), Some(value)) =>
    Some(
      Format.addCommas(
        Js.Float.toFixedWithPrecision(value, ~digits=currency.decimals),
      )
      |> addCurrencySymbol(currency),
    )
  | _ => None
  };

let toStringWithCommasRounded =
    (currency: option(t), value: option(float)): option(string) => {
  switch (currency, value) {
  | (Some(currency), Some(value)) =>
    Some(
      Format.addCommas(
        value > 100.
          ? Js.Float.toFixedWithPrecision(value, ~digits=0)
          : value |> Format.truncateDecimals(currency.decimals),
      )
      |> addCurrencySymbol(currency),
    )
  | _ => None
  };
};

let findBySymbol = (symbol: string, currencies: list(t)): option(t) =>
  currencies |> List.find_opt((currency: t) => currency.symbol === symbol);

let findBySymbolOrDefault = (symbol: string, currencies: list(t)): option(t) =>
  switch (currencies |> findBySymbol(symbol)) {
  | Some(current) => Some(current)
  | _ =>
    currencies |> List.find_opt((currency: t) => currency.symbol === default)
  };
