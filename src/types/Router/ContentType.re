type t =
  | TokenPairs
  | Accounts;

let default = TokenPairs;

let getTitle = (t: t): string =>
  switch (t) {
  | TokenPairs => "TOKEN PAIRS"
  | Accounts => "ACCOUNTS"
  };
