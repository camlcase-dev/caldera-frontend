type t =
  | Liquidity
  | Volume
  | Rewards;

let default = Liquidity;

let getTitle = (t: t): string =>
  switch (t) {
  | Liquidity => "Liquidity"
  | Volume => "Volume"
  | Rewards => "Rewards"
  };

let toParam = (t: t): Param.t => {
  name: "tab",
  value:
    switch (t) {
    | Liquidity => "liquidity"
    | Volume => "volume"
    | Rewards => "rewards"
    },
};

let fromParamValue = (paramValue: option(string)): t =>
  switch (paramValue) {
  | Some("volume") => Volume
  | Some("rewards") => Rewards
  | _ => default
  };
