type t = option(string);

let getToken = (t: t): string => {
  t |> Utils.map(Js.String.replace("XTZ/", "")) |> Utils.orDefault("");
};

let getHash = (t: t): string =>
  t
  |> Utils.map(t => "#" ++ t |> Js.String.replace("/", "_"))
  |> Utils.orDefault("");
