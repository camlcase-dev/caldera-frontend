type t = string;

let canDecodeBS58: string => bool = [%bs.raw
  {|
   function (value) {
     const bs58check = require('bs58check');
     if (bs58check.decodeUnsafe(value)) {
       return true;
     } else {
       return false;
     };
   }
  |}
];

let requiredLength = 36;

let isTooLong = (t: t): bool => String.length(t) > requiredLength;
let hasRequiredLength = (t: t): bool => String.length(t) === requiredLength;

let isValid = (t: t): bool =>
  hasRequiredLength(t)
  && canDecodeBS58(t)
  && (
    Js.String2.startsWith(t, "KT1")
    || Js.String2.startsWith(t, "tz1")
    || Js.String2.startsWith(t, "tz2")
    || Js.String2.startsWith(t, "tz3")
  );

let toDisplay = (t: t): string =>
  (t |> Js.String.substring(~from=0, ~to_=8))
  ++ "..."
  ++ (t |> Js.String.substringToEnd(~from=(t |> String.length) - 6));

let isPinned = (accounts: list(t), t: t): bool => {
  accounts |> Array.of_list |> Js.Array.includes(t);
};
