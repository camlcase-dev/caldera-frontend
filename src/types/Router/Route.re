type t =
  | Overall(TokenPair.t)
  | Account(Account.t, TokenPair.t);

let toContentType = (route: t): option(ContentType.t) =>
  switch (route) {
  | Account(_, _) => Some(Accounts)
  | Overall(Some(_)) => Some(TokenPairs)
  | _ => None
  };

let isSameType = (route1: t, route2: t): bool =>
  switch (route1, route2) {
  | (Overall(_), Overall(_))
  | (Account(_), Account(_)) => true
  | _ => false
  };
