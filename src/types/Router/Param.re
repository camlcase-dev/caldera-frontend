type t = {
  name: string,
  value: string,
};

let findByName = (name: string, params: list(t)) =>
  params |> List.find_opt((t: t) => t.name === name);

let removeParam = (name: string, params: list(t)) => {
  params |> List.filter((t: t) => t.name !== name);
};

let upsertParam = (name: string, value: string, params: list(t)) => {
  List.concat([params |> removeParam(name), [{name, value}]]);
};

let toDictElement = (t: t): (Js.Dict.key, Js.Json.t) => (
  t.name,
  Js.Json.string(t.value),
);

let toQueryString = (params: list(t)) => {
  let paramsDict = params |> List.map(toDictElement) |> Js.Dict.fromList;

  "?" ++ Qs.stringify(paramsDict);
};
