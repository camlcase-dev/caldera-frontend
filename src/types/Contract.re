type t = {
  id: string,
  decimals: int,
  name: string,
  lqtTotal: float,
  tokenPool: float,
  xtzPool: float,
};

let findByName = (name: string, contracts: list(t)): option(t) =>
  contracts |> List.find_opt(contract => contract.name === name);

let findByContractId = (contractId: string, contracts: list(t)): option(t) =>
  contracts |> List.find_opt(contract => contract.id === contractId);

let filterByText = (text: string, contracts: list(t)): list(t) => {
  let text = text |> Js.String.toLowerCase;

  text === ""
    ? contracts
    : contracts
      |> List.filter(contract => {
           contract.name |> Js.String.toLowerCase |> Js.String.includes(text)
         });
};

let sortByXtzPool = (contracts: list(t)): list(t) =>
  contracts
  |> List.sort((contract1, contract2) => {
       contract2.xtzPool -. contract1.xtzPool |> int_of_float
     });

let hasPositiveXTZPoolValue = (t: t): bool => t.xtzPool > 0.;

let getToken = (t: t): string => t.name |> Js.String.replace("XTZ/", "");

let getSubpath = (t: option(t)) =>
  t |> Utils.map(t => "/" ++ t.id) |> Utils.orDefault("");

let tokenToXtzRate = (t: t): float => {
  t.xtzPool /. t.tokenPool;
};

let getExchangeRateToXtz = (t: t): string => {
  "1 "
  ++ (t |> getToken)
  ++ " = "
  ++ (t |> tokenToXtzRate |> Printf.sprintf("%.8f"))
  ++ " XTZ";
};

let xtzToTokenRate = (t: t): float => t.tokenPool /. t.xtzPool;

let getExchangeRateFromXtz = (t: t): string =>
  "1 XTZ = "
  ++ (t |> xtzToTokenRate |> Printf.sprintf("%.8f"))
  ++ " "
  ++ (t |> getToken);

let getTokensPooled = (t: t) =>
  (t.xtzPool |> Format.truncateDecimals(2) |> Format.addCommas)
  ++ " XTZ / "
  ++ (
    t.tokenPool
    |> Format.truncateDecimals(t |> getToken === "tzBTC" ? 4 : 2)
    |> Format.addCommas
  )
  ++ " "
  ++ (t |> getToken);

let decode = (json: Js.Json.t): t =>
  Json.Decode.{
    id: json |> field("contract", string),
    decimals: json |> field("decimals", int),
    name: json |> field("name", string),
    lqtTotal: json |> Token.decodeFloatFromTokenString("lqtTotal", 0),
    tokenPool:
      json
      |> Token.decodeFloatFromTokenString(
           "tokenPool",
           json |> field("decimals", int),
         ),
    xtzPool: json |> Token.decodeFloatFromTokenString("xtzPool", 6),
  };
