type hideFor = {
  name: string,
  values: list(string),
};

type t = {
  hideFor: list(hideFor),
  isDefault: bool,
  label: string,
  value: string,
};

let findByValue = (value: string, options: list(t)): option(t) => {
  options |> List.find_opt((option: t) => option.value === value);
};

let getDefault = (options: list(t)): t => {
  options
  |> List.find_opt((option: t) => option.isDefault)
  |> Utils.orDefault(List.nth(options, 0));
};

let getDefaultValue = (options: list(t)): string =>
  (options |> getDefault).value;

let isHiddenForParams = (params: list(Param.t), option: t) =>
  params
  |> Array.of_list
  |> Js.Array.some((param: Param.t) =>
       option.hideFor
       |> Array.of_list
       |> Js.Array.some((hideFor: hideFor) =>
            hideFor.name === param.name
            && {
              hideFor.values
              |> Array.of_list
              |> Js.Array.includes(param.value);
            }
          )
     );

let filterHidden = (params, options: list(t)): list(t) =>
  options
  |> List.filter((option: t) => !(option |> isHiddenForParams(params)));

let getCurrentOption =
    (params: list(Param.t), name: string, options: list(t)): t =>
  params
  |> Param.findByName(name)
  |> Utils.flatMap((param: Param.t) => options |> findByValue(param.value))
  |> Utils.orDefault(options |> getDefault);

let toSimpleListOptions = (options: list(t)): list(SimpleList.t) =>
  options
  |> List.map((t: t) => ({label: t.label, value: t.value}: SimpleList.t));
