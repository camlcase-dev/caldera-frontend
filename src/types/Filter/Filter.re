type t = {
  name: string,
  options: list(Filter_Option.t),
};

let findByName = (name: string, filters: list(t)): option(t) =>
  filters |> List.find_opt((filter: t) => filter.name === name);
