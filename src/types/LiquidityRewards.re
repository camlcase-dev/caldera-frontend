let decodeTokenReward =
    (contracts: list(Contract.t), json: Js.Json.t): (float, float) => {
  let contract =
    contracts
    |> Contract.findByContractId(
         json |> Json.Decode.(field("dexterContractId", string)),
       );

  Belt.Option.mapWithDefault(
    contract,
    (0., 0.),
    contract => {
      let tokenReward =
        json
        |> Token.decodeFloatFromTokenFloat("tokenReward", contract.decimals);

      (tokenReward, tokenReward *. (contract |> Contract.tokenToXtzRate));
    },
  );
};

let decodeTotalInXtz = (contracts: list(Contract.t), json: Js.Json.t): float =>
  (json |> Token.decodeFloatFromTokenFloat("xtzReward", 6))
  +. snd(json |> decodeTokenReward(contracts));

let decoder = (contracts: option(list(Contract.t)), json: Js.Json.t) =>
  Belt.Option.mapWithDefault(contracts, 0., contracts =>
    json |> decodeTotalInXtz(contracts)
  );

let config = (contracts, currentContract): Config.t(float) => {
  contractIdFieldName: "dexterContractId",
  currentSubpath: (currentContract |> Contract.getSubpath) ++ "/current",
  dateFieldName: "",
  decoder: json => json |> decoder(contracts),
  path: "liquidity/rewards",
};
