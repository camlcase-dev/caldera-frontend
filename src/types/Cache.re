type t = {
  expires: MomentRe.Moment.t,
  json: Js.Json.t,
  path: string,
};

let load = (): list(t) => {
  LocalStorage.Cache.get()
  |> Utils.map(value =>
       value
       |> Json.Decode.(
            list(json =>
              switch (
                json |> field("path", string),
                json |> field("expires", string),
                json
                |> field("json", json =>
                     json
                     |> string
                     |> Json.parse
                     |> Utils.orDefault(Json.Encode.object_([]))
                   ),
              ) {
              | (path, expires, json) =>
                MomentRe.Moment.isAfter(
                  MomentRe.moment(expires)
                  |> MomentRe.Moment.add(
                       ~duration=MomentRe.duration(120000., `milliseconds),
                     ),
                  MomentRe.momentNow(),
                )
                  ? Some({expires: MomentRe.moment(expires), json, path})
                  : None
              | exception _ => None
              }
            )
          )
       |> Utils.filterNone
     )
  |> Utils.orDefault([]);
};

let save = (t: list(t)) =>
  LocalStorage.Cache.set(
    t
    |> List.map(t =>
         switch (t.expires |> MomentRe.Moment.toJSON) {
         | Some(expires) =>
           Json.Encode.(
             Some(
               object_([
                 ("path", t.path |> string),
                 ("expires", expires |> string),
                 ("json", t.json |> Json.stringify |> string),
               ]),
             )
           )
         | _ => None
         }
       )
    |> Utils.filterNone
    |> Array.of_list
    |> Json.Encode.jsonArray,
  );

let expiresAt = expiresInMilliseconds =>
  MomentRe.momentNow()
  |> MomentRe.Moment.add(
       ~duration=
         MomentRe.duration(
           expiresInMilliseconds |> float_of_int,
           `milliseconds,
         ),
     );

let getByPath = (path: string, cache): option(t) =>
  cache
  |> List.find_opt(c => c.path === path)
  |> Utils.flatMap(c =>
       MomentRe.Moment.isAfter(c.expires, MomentRe.momentNow())
         ? Some(c) : None
     );

let getJsonByPath = (path: string, cache): option(Js.Json.t) =>
  cache
  |> List.find_opt(c => c.path === path)
  |> Utils.flatMap(c =>
       MomentRe.Moment.isAfter(
         c.expires
         |> MomentRe.Moment.add(
              ~duration=MomentRe.duration(120000., `milliseconds),
            ),
         MomentRe.momentNow(),
       )
         ? Some(c.json) : None
     );

let upsert = (newCache, cache) =>
  cache
  |> Array.of_list
  |> Js.Array.some(cache => cache.path === newCache.path)
    ? cache
      |> List.map(cache => cache.path === newCache.path ? newCache : cache)
    : List.concat([cache, [newCache]]);
