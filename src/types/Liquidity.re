type t = float;

let config: Config.t(t) = {
  contractIdFieldName: "Contract",
  currentSubpath: "/current",
  dateFieldName: "Day",
  decoder: json =>
    (json |> Token.decodeFloatFromTokenString("XtzPool", 6)) *. 2.,
  path: "liquidity",
};
