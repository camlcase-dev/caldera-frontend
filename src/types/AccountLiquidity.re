type t = {
  lqt: float,
  lqtPercentage: float,
  xtzPool: float,
  tokenPool: float,
};

let sumFn = (values: array(t)): t => {
  values
  |> Js.Array.reduce(
       (acc, cur) =>
         {
           lqt: acc.lqt +. cur.lqt,
           lqtPercentage: acc.lqtPercentage +. cur.lqtPercentage,
           xtzPool: acc.xtzPool +. cur.xtzPool,
           tokenPool: acc.tokenPool +. cur.tokenPool,
         },
       {lqt: 0., lqtPercentage: 0., xtzPool: 0., tokenPool: 0.},
     );
};

let decoder = (contracts: option(list(Contract.t)), json: Js.Json.t): t =>
  switch (contracts) {
  | Some(contracts) =>
    let lqt = json |> Token.decodeFloatFromTokenString("lqt", 0);
    let contract =
      contracts
      |> Contract.findByContractId(
           json |> Json.Decode.(field("dexterContractId", string)),
         );

    Belt.Option.mapWithDefault(
      contract,
      {lqt: 0., lqtPercentage: 0., xtzPool: 0., tokenPool: 0.},
      contract => {
        let lqtPercentage = lqt /. contract.lqtTotal;

        let xtzPool =
          contract.xtzPool
          *. lqtPercentage
          |> Js.Float.toFixedWithPrecision(~digits=6)
          |> float_of_string;

        let tokenPool =
          contract.tokenPool
          *. lqtPercentage
          |> Js.Float.toFixedWithPrecision(~digits=contract.decimals)
          |> float_of_string;

        let lqtPercentage = lqtPercentage *. 100.;

        {lqt, lqtPercentage, xtzPool, tokenPool};
      },
    );
  | None => {lqt: 0., lqtPercentage: 0., xtzPool: 0., tokenPool: 0.}
  };

let config = (contracts, account): Config.t(t) => {
  contractIdFieldName: "dexterContractId",
  currentSubpath: "/account/" ++ account ++ "/current",
  dateFieldName: "",
  decoder: decoder(contracts),
  path: "liquidity",
};
