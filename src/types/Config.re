type t('a) = {
  contractIdFieldName: string,
  currentSubpath: string,
  dateFieldName: string,
  decoder: Json.Decode.decoder('a),
  path: string,
};
