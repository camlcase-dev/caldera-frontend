type t = (MomentRe.Moment.t, float);

let equal = (t1: t, t2: t): bool => {
  fst(t1) === fst(t2) && snd(t1) === snd(t2);
};
