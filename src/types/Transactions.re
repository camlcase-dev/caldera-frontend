let useTransactionsNumber =
    (~account: option(string)=?, ~cacheOnly=false, ()) =>
  Api.useRequestWithCache(
    ~path=
      "transactions"
      ++ (
        account
        |> Utils.map(account => "/account/" ++ account)
        |> Utils.orDefault("")
      )
      ++ "/current",
    ~decoder=json => Json.Decode.(json |> int) |> string_of_int,
    ~cacheOnly,
    (),
  );
