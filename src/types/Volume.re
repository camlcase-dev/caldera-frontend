type t = float;

let config: Config.t(t) = {
  contractIdFieldName: "Contract",
  currentSubpath: "/current",
  dateFieldName: "Date",
  decoder: Token.decodeFloatFromTokenString("Total", 6),
  path: "volume",
};
