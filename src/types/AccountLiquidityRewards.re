type t = {
  xtzReward: float,
  tokenReward: float,
  tokenRewardInXtz: float,
  totalReward: float,
};

let sumFn = (values: array(t)): t => {
  values
  |> Js.Array.reduce(
       (acc, cur) =>
         {
           xtzReward: acc.xtzReward +. cur.xtzReward,
           tokenReward: acc.tokenReward +. cur.tokenReward,
           tokenRewardInXtz: acc.tokenRewardInXtz +. cur.tokenRewardInXtz,
           totalReward: acc.totalReward +. cur.totalReward,
         },
       {
         xtzReward: 0.,
         tokenReward: 0.,
         tokenRewardInXtz: 0.,
         totalReward: 0.,
       },
     );
};

let decodeTokenReward =
    (contracts: list(Contract.t), json: Js.Json.t): (float, float) => {
  let contract =
    contracts
    |> Contract.findByContractId(
         json |> Json.Decode.(field("dexterContractId", string)),
       );

  Belt.Option.mapWithDefault(
    contract,
    (0., 0.),
    contract => {
      let tokenReward =
        json
        |> Token.decodeFloatFromTokenString("tokenReward", contract.decimals);

      (tokenReward, tokenReward *. (contract |> Contract.tokenToXtzRate));
    },
  );
};

let decoder = (contracts: option(list(Contract.t)), json: Js.Json.t): t =>
  switch (contracts) {
  | Some(contracts) =>
    let xtzReward = json |> Token.decodeFloatFromTokenString("xtzReward", 6);
    let (tokenReward, tokenRewardInXtz) =
      json |> decodeTokenReward(contracts);

    {
      xtzReward,
      tokenReward,
      tokenRewardInXtz,
      totalReward: xtzReward +. tokenRewardInXtz,
    };
  | None => {
      xtzReward: 0.,
      tokenReward: 0.,
      tokenRewardInXtz: 0.,
      totalReward: 0.,
    }
  };

let config = (contracts, account): Config.t(t) => {
  contractIdFieldName: "dexterContractId",
  currentSubpath: "/account/" ++ account ++ "/current",
  dateFieldName: "",
  decoder: decoder(contracts),
  path: "liquidity/rewards",
};
