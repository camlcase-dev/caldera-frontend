type t = {
  cache: list(Cache.t),
  setCache: (list(Cache.t) => list(Cache.t)) => unit,
};

let defaultState: t = {cache: [], setCache: _ => ()};

let ctx = React.createContext(defaultState);

module ProviderInternal = {
  let makeProps = (~value, ~children, ()) => {
    "value": value,
    "children": children,
  };

  let make = React.Context.provider(ctx);
};

module Provider = {
  [@react.component]
  let make = (~children: React.element) => {
    let (cache, setCache) = React.useState(_ => Cache.load());

    React.useEffect1(
      () => {
        Cache.save(cache);
        Some(_ => Cache.save(cache));
      },
      [|cache|],
    );

    <ProviderInternal value={cache, setCache}> children </ProviderInternal>;
  };
};

let useContext = () => React.useContext(ctx);
