type t = {
  accounts: list(Account.t),
  pinAccount: Account.t => unit,
  unpinAccount: Account.t => unit,
};

let defaultState: t = {
  accounts: [],
  pinAccount: _ => (),
  unpinAccount: _ => (),
};

let ctx = React.createContext(defaultState);

module ProviderInternal = {
  let makeProps = (~value, ~children, ()) => {
    "value": value,
    "children": children,
  };

  let make = React.Context.provider(ctx);
};

module Provider = {
  [@react.component]
  let make = (~children: React.element) => {
    let (accounts, setAccounts) =
      React.useState(_ => LocalStorage.Accounts.get());

    let pinAccount =
      React.useCallback1(
        (account: Account.t) => {
          setAccounts(_ => List.concat([[account], accounts]))
        },
        [|accounts|],
      );

    let unpinAccount =
      React.useCallback1(
        (account: Account.t) => {
          setAccounts(_ => accounts |> List.filter(a => a !== account))
        },
        [|accounts|],
      );

    React.useEffect1(
      () => {
        LocalStorage.Accounts.set(accounts);
        None;
      },
      [|accounts|],
    );

    <ProviderInternal value={accounts, pinAccount, unpinAccount}>
      children
    </ProviderInternal>;
  };
};

let useContext = () => React.useContext(ctx);
