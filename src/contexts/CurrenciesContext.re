type t = {
  currencies: list(Currency.t),
  currentCurrency: option(Currency.t),
  setCurrentCurrency: Currency.t => unit,
  currenciesError: bool,
};

let defaultValue: t = {
  currencies: [],
  currentCurrency: None,
  setCurrentCurrency: _ => (),
  currenciesError: false,
};

let ctx = React.createContext(defaultValue);

module ProviderInternal = {
  let makeProps = (~value, ~children, ()) => {
    "value": value,
    "children": children,
  };

  let make = React.Context.provider(ctx);
};

module Provider = {
  [@react.component]
  let make = (~children: React.element) => {
    let redirectToParam = Router.useRedirectToParam();
    let currencyParam = Router.useParamValue("currency");
    let (currenciesError, setError) = React.useState(_ => false);
    let (currentCurrency, setCurrentCurrency) = React.useState(_ => None);

    let currencies =
      CoinGecko.useCurrencies(() => setError(_ => true))
      |> Utils.orDefault([]);

    React.useEffect2(
      () => {
        setCurrentCurrency(_ =>
          switch (
            currencyParam
            |> Utils.map(currencyParam =>
                 currencies |> Currency.findBySymbol(currencyParam)
               )
          ) {
          | Some(currency) => currency
          | _ =>
            currencies
            |> Currency.findBySymbolOrDefault(LocalStorage.Currency.get())
          }
        );
        None;
      },
      (currencies, currencyParam),
    );

    React.useEffect1(
      () => {
        switch (currentCurrency) {
        | Some((currentCurrency: Currency.t))
            when currentCurrency.symbol !== Currency.default =>
          redirectToParam({name: "currency", value: currentCurrency.symbol})
        | _ => ()
        };
        None;
      },
      [|currentCurrency|],
    );

    let setCurrentCurrency =
      React.useCallback((currency: Currency.t) => {
        LocalStorage.Currency.set(currency.symbol);
        setCurrentCurrency(_ => Some(currency));
        redirectToParam({name: "currency", value: currency.symbol});
      });

    <ProviderInternal
      value={currencies, currentCurrency, setCurrentCurrency, currenciesError}>
      children
    </ProviderInternal>;
  };
};

let useContext = () => React.useContext(ctx);

let useValueInCurrency = (value: option(float)) => {
  let {currentCurrency} = useContext();

  React.useMemo2(
    () =>
      switch (currentCurrency, value) {
      | (Some(currentCurrency), _) when currentCurrency |> Currency.isXTZ => value
      | (Some(currentCurrency), Some(value)) =>
        Some(value *. currentCurrency.rate)
      | _ => None
      },
    (currentCurrency, value),
  );
};

let useValueInCurrencyString = (value: option(float)) => {
  let {currentCurrency} = useContext();
  let value = value |> useValueInCurrency;

  React.useMemo2(
    () => value |> Currency.toStringWithCommas(currentCurrency),
    (currentCurrency, value),
  );
};

let useValueInCurrencyStringRounded = (value: option(float)) => {
  let {currentCurrency} = useContext();
  let value = value |> useValueInCurrency;

  React.useMemo2(
    () => value |> Currency.toStringWithCommasRounded(currentCurrency),
    (currentCurrency, value),
  );
};
