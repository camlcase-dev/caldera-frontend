type t = {
  currentContract: option(Contract.t),
  currentContractIsLoading: bool,
  contracts: option(list(Contract.t)),
};

let defaultState: t = {
  currentContract: None,
  currentContractIsLoading: true,
  contracts: None,
};

let ctx = React.createContext(defaultState);

module ProviderInternal = {
  let makeProps = (~value, ~children, ()) => {
    "value": value,
    "children": children,
  };

  let make = React.Context.provider(ctx);
};

module Provider = {
  [@react.component]
  let make = (~children: React.element) => {
    let clearHash = Router.useClearHash();

    let contracts =
      Api.useRequestWithCache(
        ~path="contracts",
        ~decoder=json => json |> Json.Decode.list(Contract.decode),
        ~expiresInMilliseconds=300000,
        ~fetchOnMount=true,
        (),
      );

    let contracts =
      React.useMemo1(
        () =>
          contracts
          |> Utils.map(List.filter(Contract.hasPositiveXTZPoolValue)),
        [|contracts|],
      );

    let tokenPair = Router.useTokenPair();

    let currentContract =
      React.useMemo2(
        () =>
          tokenPair
          |> Utils.flatMap(tokenPair =>
               contracts
               |> Utils.flatMap(contracts =>
                    switch (contracts |> Contract.findByName(tokenPair)) {
                    | Some(contract) => Some(contract)
                    | _ =>
                      clearHash();
                      None;
                    }
                  )
             ),
        (contracts, tokenPair),
      );

    let currentContractIsLoading =
      currentContract
      |> Utils.map((contract: Contract.t) => contract.name) !== tokenPair;

    <ProviderInternal
      value={currentContract, currentContractIsLoading, contracts}>
      children
    </ProviderInternal>;
  };
};

let useContext = () => React.useContext(ctx);
