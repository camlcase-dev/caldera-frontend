type t = {showMessage: string => unit};

let defaultState: t = {showMessage: _ => ()};

let ctx = React.createContext(defaultState);

module ProviderInternal = {
  let makeProps = (~value, ~children, ()) => {
    "value": value,
    "children": children,
  };

  let make = React.Context.provider(ctx);
};

module Provider = {
  [@react.component]
  let make = (~children: React.element) => {
    let (message, setMessage) = React.useState(_ => None);
    let timeoutId = React.useRef(None);

    React.useEffect1(
      () => {
        timeoutId.current
        |> Utils.map((timeoutId, _) => Js.Global.clearTimeout(timeoutId))
      },
      [|timeoutId|],
    );

    let showMessage =
      React.useCallback1(
        message => {
          timeoutId.current
          |> Utils.map(timeoutId => Js.Global.clearTimeout(timeoutId))
          |> Utils.orDefault();
          setMessage(_ => Some(message));
          timeoutId.current =
            Some(Js.Global.setTimeout(() => setMessage(_ => None), 2000));
        },
        [|timeoutId|],
      );

    <ProviderInternal value={showMessage: showMessage}>
      children
      {message
       |> Utils.renderOpt(message =>
            <Flex
              background=Colors.white
              borderRadius={`px(8)}
              p={`px(12)}
              className=Css.(
                style([
                  boxShadow(
                    Shadow.box(
                      ~y=px(2),
                      ~blur=px(10),
                      rgba(0, 0, 0, `num(0.25)),
                    ),
                  ),
                ])
              )
              position=`fixed
              bottom={`px(8)}
              right={`px(8)}>
              <Text textStyle=TextStyles.message>
                {message |> React.string}
              </Text>
            </Flex>
          )}
    </ProviderInternal>;
  };
};

let useContext = () => React.useContext(ctx);
