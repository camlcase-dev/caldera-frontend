type t = {
  value: Bigint.t,
  decimals: int,
};

let mkToken = (value, decimals) => {value, decimals};

let rec repeatString = (n, s) => n == 0 ? "" : s ++ repeatString(n - 1, s);

let toBigint = (t: t): Bigint.t => t.value;

let toString = (t: t): string => {
  let s = Bigint.to_string(t.value);
  let sl = String.length(s);

  (
    if (t.decimals == 0 || s === "0") {
      s;
    } else if (sl <= t.decimals) {
      "0." ++ ("0" |> repeatString(t.decimals - sl)) ++ s;
    } else {
      let l = Js.String.slice(~from=0, ~to_=sl - t.decimals, s);
      let r = Js.String.slice(~from=sl - t.decimals, ~to_=sl, s);
      l ++ "." ++ r;
    }
  )
  |> Js.Float.fromString
  |> Js.Float.toString;
};

/* For example token value 1.005 has 3 decimals, concert it to float 1.005  */
let toFloatWithDecimal = (t: t): float => Js.Float.fromString(toString(t));

let toStringWithCommas = (t: t): string => Format.addCommas(toString(t));

let compare = (x, y) => Bigint.compare(x.value, y.value);
let equal = (x, y) => compare(x, y) == 0;

let decodeFloatFromTokenString =
    (fieldName: string, decimals: int, json: Js.Json.t) =>
  toFloatWithDecimal(
    mkToken(
      json |> Json.Decode.(field(fieldName, string)) |> Bigint.of_string,
      decimals,
    ),
  );

let decodeFloatFromTokenFloat =
    (fieldName: string, decimals: int, json: Js.Json.t) =>
  toFloatWithDecimal(
    mkToken(
      json
      |> Json.Decode.(field(fieldName, Json.Decode.float))
      |> Bigint.of_float,
      decimals,
    ),
  );
