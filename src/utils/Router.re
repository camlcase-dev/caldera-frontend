let pathToString = (path: list(string)) =>
  "/" ++ (path |> Array.of_list |> Js.Array.joinWith("/"));

let addWithSymbol = (symbol: string, value: string) =>
  value !== "" ? symbol ++ value : "";

let useParams = (): list(Param.t) => {
  let url = ReasonReactRouter.useUrl();
  let defaultParams = GraphFilters_Helpers.getDefaultParams();

  let params =
    switch (url.search |> Qs.parse |> Js.Json.decodeObject) {
    | Some(paramsDict) =>
      paramsDict
      |> Js.Dict.entries
      |> Js.Array.map(((name, value)) =>
           ({name, value: Json.Decode.string(value)}: Param.t)
         )
      |> Array.to_list
    | _ => []
    };

  List.concat([
    defaultParams
    |> List.filter((defaultParam: Param.t) =>
         !(
           params
           |> Array.of_list
           |> Js.Array.some((param: Param.t) =>
                param.name === defaultParam.name
              )
         )
       ),
    params,
  ]);
};

let useGraphFilterValue = (name: string): string => {
  let url = ReasonReactRouter.useUrl();
  let params = useParams();

  url.search
  |> Qs.parse
  |> Json.Decode.optional(Json.Decode.field(name, Json.Decode.string))
  |> GraphFilters_Helpers.orDefault(name, params);
};

let useParamValue = (name: string): option(string) => {
  let url = ReasonReactRouter.useUrl();

  switch (
    url.search
    |> Qs.parse
    |> Json.Decode.optional(Json.Decode.field(name, Json.Decode.string))
  ) {
  | Some(value) => Some(value)
  | _ => None
  };
};

let useRedirectToParam = () => {
  let {search, hash} = ReasonReactRouter.useUrl();
  let params = useParams();

  React.useCallback2(
    ({name, value}: Param.t) => {
      let updatedParams = params |> Param.upsertParam(name, value);

      let params =
        GraphFilters_Helpers.isGraphFilter(name)
          ? updatedParams
            |> List.map((param: Param.t) =>
                 param.name !== name
                 && GraphFilters_Helpers.isGraphFilter(param.name)
                 && !
                      GraphFilters_Helpers.isOptionAvailable(
                        param,
                        updatedParams,
                      )
                   ? {
                     ...param,
                     value:
                       GraphFilters_Helpers.getDefaultValueByFilterNameAndParams(
                         param.name,
                         updatedParams,
                       ),
                   }
                   : param
               )
          : updatedParams;

      (params |> Param.toQueryString)
      ++ addWithSymbol("#", hash)
      |> ReasonReactRouter.push;
    },
    (hash, search),
  );
};

let useCurrentRoute = (): Route.t => {
  let {path, hash} = ReasonReactRouter.useUrl();
  let tokenPair =
    hash !== "" ? Some(hash |> Js.String.replace("_", "/")) : None;

  switch (path) {
  | [accountId] when accountId |> Account.isValid =>
    Account(accountId, tokenPair)
  | _ => Overall(tokenPair)
  };
};

let useContentType = (): option(ContentType.t) => {
  let currentRoute = useCurrentRoute();
  currentRoute |> Route.toContentType;
};

let useRedirectToRoute = () => {
  let currentRoute = useCurrentRoute();
  let params = useParams();

  React.useCallback2(
    (route: Route.t) => {
      let url =
        switch (route) {
        | Overall(_) => "/"
        | Account(account, _) => "/" ++ account
        };

      let queryString =
        (
          Route.isSameType(currentRoute, route)
            ? params : params |> Param.removeParam("tab")
        )
        |> Param.toQueryString;

      let hash =
        switch (route) {
        | Overall(tokenPair)
        | Account(_, tokenPair) => tokenPair |> TokenPair.getHash
        };

      url ++ queryString ++ hash |> ReasonReactRouter.push;
    },
    (currentRoute, params),
  );
};

let useSubcontentType = (): SubcontentType.t => {
  let paramValue = useParamValue("tab");
  paramValue |> SubcontentType.fromParamValue;
};

let useTokenPair = (): TokenPair.t => {
  let currentRoute = useCurrentRoute();

  switch (currentRoute) {
  | Overall(tokenPair)
  | Account(_, tokenPair) => tokenPair
  };
};

let useCurrentAccount = (): option(Account.t) => {
  let currentRoute = useCurrentRoute();

  switch (currentRoute) {
  | Account(accountId, _) => Some(accountId)
  | _ => None
  };
};

let useClearHash = () => {
  let url = ReasonReactRouter.useUrl();

  React.useCallback1(
    () =>
      (url.path |> pathToString)
      ++ addWithSymbol("?", url.search)
      |> ReasonReactRouter.push,
    [|url|],
  );
};
