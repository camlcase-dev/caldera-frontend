type t;

[@bs.module]
external make:
  (
  [@bs.unwrap]
  [ | `Str(string) | `Int(int) | `Float(float) | `Numeral(t)]
  ) =>
  t =
  "numeral";

[@bs.send] external format: (t, ~format: string=?, unit) => string = "format";
