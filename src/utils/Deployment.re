type t =
  | Production
  | Staging
  | Development;

let ofRaw: unit => string = [%bs.raw
  {|
     function () {
        if (PROD) {
          return "production";
        } else if (STAG) {
          return "staging";
        } else  {
          return "development";
        }
      }
    |}
];

let get = () =>
  switch (ofRaw()) {
  | "production" => Production
  | "staging" => Staging
  | _ => Development
  };
