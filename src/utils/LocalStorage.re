let setItemRaw: (string, string) => unit = [%bs.raw
  {|
    function (key, value) {
        return localStorage.setItem(key, value);
    }
    |}
];

let setStringItem = (key: string, value: string): unit =>
  setItemRaw(key, value);

let setItem = (key: string, value: Js.Json.t): unit =>
  setItemRaw(key, Js.Json.stringify(value));

let getItemRaw: string => Js.Nullable.t(string) = [%bs.raw
  {|
    function (key) {
        return localStorage.getItem(key);
    }
    |}
];

let getItem = (key: string): option(Js.Json.t) =>
  switch (
    Js.Nullable.toOption(getItemRaw(key))
    |> Utils.map(value => Js.Json.parseExn(value))
  ) {
  | value => value
  | exception _ => None
  };

let getStringItem = (key: string): option(string) =>
  Js.Nullable.toOption(getItemRaw(key));

module Currency = {
  let key = "caldera:currency";
  let set = (currency: string) => setStringItem(key, currency);
  let get = () => getStringItem(key) |> Utils.orDefault(Currency.default);
};

module Accounts = {
  let key = "caldera:accounts";
  let set = (accounts: list(string)) =>
    setItem(key, accounts |> Array.of_list |> Json.Encode.stringArray);
  let get = () =>
    getItem(key)
    |> Utils.map(value => value |> Json.Decode.list(Json.Decode.string))
    |> Utils.orDefault([]);
};

module Cache = {
  let key = "caldera:cache";
  let set = cache => setItem(key, cache);
  let get = () => getItem(key);
};
