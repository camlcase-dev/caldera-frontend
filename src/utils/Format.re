let addBrackets = (value: string): string => "(" ++ value ++ ")";
let addBracketsToDisplay = (value: string): React.element =>
  value |> addBrackets |> React.string;

let addToken = (token: string, value: string): string =>
  value ++ " " ++ token;
let addTokenToDisplay = (token: string, value: string): React.element =>
  value |> addToken(token) |> React.string;

let truncateDecimals = (~sign=true, decimals: int, value: float): string => {
  let minValue =
    "0."
    ++ (
      Belt.Array.range(1, decimals)
      |> Array.map(i => i === decimals ? "1" : "0")
      |> Js.Array.joinWith("")
    );

  switch (value) {
  | 0.0 => "0"
  | _ when value < (minValue |> float_of_string) =>
    (sign ? "<" : "") ++ minValue
  | _ => Js.Float.toFixedWithPrecision(value, ~digits=decimals)
  };
};

let tokenToDisplay = (tokenPool: option(float), token: string) =>
  tokenPool
  |> Utils.map(tokenPool =>
       tokenPool |> truncateDecimals(6) |> addTokenToDisplay(token)
     );

let addCommas: string => string = [%bs.raw
  {|
    function (s) {
      var sp = s.split(".");
      var l = sp[0].replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
      var r = parseFloat(sp[1])
      return sp.length > 1 && r ? l.concat(".", sp[1]) : l
    }
     |}
];

let decodeMoment = (json: Js.Json.t): MomentRe.Moment.t =>
  MomentRe.momentUtc(json |> Json.Decode.string);

let dateToUnix = (t: Js.Json.t): float =>
  (
    MomentRe.momentUtc(t |> Json.Decode.string)
    |> MomentRe.Moment.toUnix
    |> float_of_int
  )
  *. 1000.;

let unixDay = 86400000.;
