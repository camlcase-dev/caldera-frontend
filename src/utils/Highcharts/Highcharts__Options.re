module Data = {
  type t;
  /** Creates a CurrentData.t value */
  [@bs.obj]
  external make: (~csv: string=?, unit) => t;
};

module CssObject = {
  type t;
  /** Creates a CssObject.t value */
  [@bs.obj]
  external make:
    (
      ~color: string=?,
      ~fontFamily: string=?,
      ~fontSize: string=?,
      ~fontWeight: string=?,
      ~letterSpacing: string=?,
      ~lineHeight: string=?,
      unit
    ) =>
    t;
};

module Chart = {
  type t;
  /** Creates a Chart.t value */
  [@bs.obj]
  external make:
    (
      ~backgroundColor: [@bs.as "transparent"] _,
      ~animation: bool=?,
      ~height: int=?,
      ~width: int=?,
      ~style: CssObject.t=?,
      ~borderRadius: int=?,
      ~margin: (int, int, int, int)=?,
      ~marginTop: int=?,
      ~marginRight: int=?,
      ~marginBottom: int=?,
      ~marginLeft: int=?,
      ~spacing: (int, int, int, int)=?,
      ~spacingTop: int=?,
      ~spacingRight: int=?,
      ~spacingBottom: int=?,
      ~spacingLeft: int=?,
      unit
    ) =>
    t;
};

module Boost = {
  type t;
  /** Creates a Boost.t value */
  [@bs.obj]
  external make: (~useGPUTranslations: bool=?, unit) => t;
};

module Tooltip = {
  type t;
  /** Creates a Tooltip.t value.
    Note: this is not the same type as the tooltip property inside a series.
   */
  [@bs.obj]
  external make:
    (
      ~backgroundColor: string=?,
      ~borderColor: string=?,
      ~borderRadius: int=?,
      ~borderWidth: int=?,
      ~className: string=?,
      ~distance: int=?,
      ~formatter: [@bs.this] (
                    {
                      .
                      "point": {
                        .
                        "x": int,
                        "y": int,
                      },
                    } =>
                    string
                  )
                    =?,
      ~headerFormat: string=?,
      ~outside: bool=?,
      ~pointFormat: string=?,
      ~pointFormatter: [@bs.this] (
                         {
                           .
                           "x": int,
                           "y": int,
                           "series": {. "name": string},
                         } =>
                         string
                       )
                         =?,
      ~positioner: (
                     int,
                     int,
                     {
                       .
                       "plotX": int,
                       "plotY": int,
                     }
                   ) =>
                   {
                     .
                     "x": int,
                     "y": int,
                   }
                     =?,
      ~padding: int=?,
      ~shape: string=?,
      ~shadow: bool=?,
      ~useHTML: bool=?,
      unit
    ) =>
    t;
};

module Title = {
  type t;
  /** Creates a CssObject.t value.
    Note this is the chart title, which has a different type than the axis title.
   */
  [@bs.obj]
  external make:
    (
      ~align: [ | `left | `center | `right]=?,
      ~style: CssObject.t=?,
      ~text: option(string), // text: undefined is needed to hide the title: https://api.highcharts.com/highcharts/yAxis.title.text
      ~x: int=?,
      ~y: int=?,
      unit
    ) =>
    t;
};

module MarkerHoverState = {
  type t;
  [@bs.obj]
  external make:
    (
      ~enabled: bool=?,
      ~fillColor: string=?,
      ~lineColor: string=?,
      ~lineWidth: int=?,
      ~lineWidthPlus: int=?,
      ~radius: int=?,
      ~radiusPlus: int=?,
      unit
    ) =>
    t;
};

module MarkerState = {
  type t;
  [@bs.obj] external make: (~hover: MarkerHoverState.t=?, unit) => t;
};

module Marker = {
  type t;
  [@bs.obj]
  external make:
    (
      ~enabled: bool=?,
      ~enabledThreshold: int=?,
      ~fillColor: string=?,
      ~height: int=?,
      ~lineColor: string=?,
      ~lineWidth: int=?,
      ~radius: int=?,
      ~symbol: string=?,
      ~width: int=?,
      ~states: MarkerState.t=?,
      unit
    ) =>
    t;
};

module Halo = {
  type t;
  [@bs.obj] external make: (~opacity: float=?, ~size: int=?, unit) => t;
};

module AreaHoverState = {
  type t;
  [@bs.obj]
  external make:
    (
      ~enabled: bool=?,
      ~lineWidth: int=?,
      ~lineWidthPlus: int=?,
      ~marker: Marker.t=?,
      ~halo: Halo.t=?,
      unit
    ) =>
    t;
};

module AreaState = {
  type t;
  [@bs.obj] external make: (~hover: AreaHoverState.t=?, unit) => t;
};

module ColumnHoverState = {
  type t;
  [@bs.obj]
  external make:
    (~borderColor: string=?, ~brightness: float=?, ~color: string=?, unit) => t;
};

module ColumnState = {
  type t;
  [@bs.obj] external make: (~hover: ColumnHoverState.t=?, unit) => t;
};

module Series = {
  type t;
  /** Creates a `line` series */
  [@bs.obj]
  external area:
    (
      ~_type: [@bs.as "area"] _,
      ~color: string,
      ~fillColor: string,
      ~lineColor: string,
      ~lineWidth: int,
      ~data: array((float, float)),
      ~states: AreaState.t=?,
      ~symbol: string=?,
      ~marker: Marker.t=?,
      ~name: string=?,
      unit
    ) =>
    t;
  /** Creates a `bar` series */
  [@bs.obj]
  external column:
    (
      ~_type: [@bs.as "column"] _,
      ~color: string,
      ~borderColor: string,
      ~groupPadding: float,
      ~pointPadding: float,
      ~data: array((float, float)),
      ~states: ColumnState.t=?,
      ~name: string=?,
      unit
    ) =>
    t;
  /** Creates a `heatmap` series */
  [@bs.obj]
  external heatmap:
    (
      ~_type: [@bs.as "heatmap"] _,
      ~boostThreshold: int=?,
      ~borderWidth: int=?,
      ~colsize: int=?,
      ~data: array(array('a))=?,
      ~nullColor: string=?,
      ~turboThreshold: float=?,
      unit
    ) =>
    t;
};

module AxisLabel = {
  type t;
  /** Creates an AxisLabel.t value */
  [@bs.obj]
  external make:
    (
      ~align: [ | `left | `center | `right]=?,
      ~format: string=?,
      ~formatter: [@bs.this] (
                    {
                      .
                      "value": float,
                      "isFirst": bool,
                      "isLast": bool,
                    } =>
                    string
                  )
                    =?,
      ~style: CssObject.t=?,
      ~staggerLines: int=?,
      ~headerFormat: string=?,
      ~pointFormat: string=?,
      ~title: Title.t=?,
      ~x: int=?,
      ~y: int=?,
      unit
    ) =>
    t;
};

module DateTimeFormat = {
  type t;
  [@bs.obj]
  external make:
    (
      ~millisecond: string=?,
      ~second: string=?,
      ~minute: string=?,
      ~hour: string=?,
      ~day: string=?,
      ~week: string=?,
      ~month: string=?,
      ~year: string=?,
      unit
    ) =>
    t;
};

module Crosshair = {
  type t;
  [@bs.obj] external make: (~color: string=?, ~dashStyle: string=?, unit) => t;
};

module Axis = {
  type t;

  /** Creates an Axis.t value */
  [@bs.obj]
  external make:
    (
      ~_type: [ | `linear | `logarithmic | `datetime | `category]=?,
      ~allowDecimals: bool=?,
      ~categories: array(string)=?,
      ~crosshair: Crosshair.t=?,
      ~dateTimeLabelFormats: DateTimeFormat.t=?,
      ~endOnTick: bool=?,
      ~gridLineWidth: int=?,
      ~labels: AxisLabel.t=?,
      ~lineWidth: int=?,
      ~lineColor: string=?,
      ~min: float=?,
      ~max: float=?,
      ~minPadding: int=?,
      ~maxPadding: int=?,
      ~offset: int=?,
      ~opposite: bool=?,
      ~reversed: bool=?,
      ~showLastLabel: bool=?,
      ~startOnTick: bool=?,
      ~tickAmount: int=?,
      ~tickColor: string=?,
      ~tickLength: int=?,
      ~tickPositions: array(int)=?,
      ~tickInterval: int=?,
      ~tickWidth: int=?,
      ~title: Title.t=?,
      unit
    ) =>
    t;
};

module ColorAxis = {
  type t;

  /** Creates a ColorAxis.t value */
  [@bs.obj]
  external make:
    (
      ~endOnTick: bool=?,
      ~labels: AxisLabel.t=?,
      ~max: int=?,
      ~min: int=?,
      ~reversed: bool=?,
      ~startOnTick: bool=?,
      ~stops: array((float, string))=?,
      ~tickAmount: int=?,
      unit
    ) =>
    t;
};

module Credits = {
  type t;

  /** Creates a Credits.t value */
  [@bs.obj]
  external make: (~enabled: bool=?, unit) => t;
};

module Exporting = {
  type t;

  /** Creates an Exporting.t value */
  [@bs.obj]
  external make: (~enabled: bool=?, unit) => t;
};

module Legend = {
  type t;
  type title;

  /** Creates a Legend.title value */
  [@bs.obj]
  external title: (~style: CssObject.t=?, ~text: string=?, unit) => title;

  /** Creates a Legend.t value */
  [@bs.obj]
  external make:
    (
      ~align: [ | `left | `center | `right]=?,
      ~enabled: bool=?,
      ~itemStyle: CssObject.t=?,
      ~itemHiddenStyle: CssObject.t=?,
      ~itemHoverStyle: CssObject.t=?,
      ~itemMarginTop: int=?,
      ~itemDistance: int=?,
      ~layout: [ | `horizontal | `vertical | `proximate]=?,
      ~symbolHeight: int=?,
      ~symbolWidth: int=?,
      ~symbolRadius: int=?,
      ~symbolPadding: int=?,
      ~title: title=?,
      ~verticalAlign: [ | `top | `middle | `bottom]=?,
      ~x: int=?,
      ~y: int=?,
      ~useHTML: bool=?,
      unit
    ) =>
    t;
};

[@bs.deriving {abstract: light}]
type make = {
  [@bs.optional]
  boost: Boost.t,
  [@bs.optional]
  chart: Chart.t,
  [@bs.optional]
  colorAxis: ColorAxis.t,
  [@bs.optional]
  credits: Credits.t,
  [@bs.optional]
  data: float,
  [@bs.optional]
  exporting: Exporting.t,
  [@bs.optional]
  legend: Legend.t,
  [@bs.optional]
  series: array(Series.t),
  [@bs.optional]
  subtitle: Title.t,
  [@bs.optional]
  title: Title.t,
  [@bs.optional]
  tooltip: Tooltip.t,
  [@bs.optional]
  xAxis: Axis.t,
  [@bs.optional]
  yAxis: Axis.t,
};
