let make = (enabled, itemDistance) =>
  Highcharts__Options.(
    Legend.make(
      ~enabled,
      ~align=`right,
      ~itemStyle=
        CssObject.make(
          ~fontFamily="Source Sans Pro",
          ~color="#" ++ Colors.whiteHex ++ "BF",
          ~fontWeight="normal",
          ~fontSize="12px",
          ~lineHeight="19px",
          (),
        ),
      ~itemHoverStyle=CssObject.make(~color="#" ++ Colors.whiteHex, ()),
      ~itemHiddenStyle=
        CssObject.make(~color="#" ++ Colors.whiteHex ++ "40", ()),
      ~itemDistance,
      ~itemMarginTop=0,
      ~symbolHeight=5,
      ~symbolRadius=0,
      ~symbolPadding=4,
      ~useHTML=true,
      (),
    )
  );
