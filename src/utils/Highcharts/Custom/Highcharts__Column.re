let make = (~seriesColor=Colors.malachiteHex, seriesName, seriesData) =>
  Highcharts__Options.(
    Series.column(
      ~borderColor="transparent",
      ~color="#" ++ seriesColor ++ "80",
      ~data=seriesData,
      ~groupPadding=0.05,
      ~name=seriesName,
      ~pointPadding=0.08,
      ~states=
        ColumnState.make(
          ~hover=ColumnHoverState.make(~color="#" ++ seriesColor, ()),
          (),
        ),
      (),
    )
  );
