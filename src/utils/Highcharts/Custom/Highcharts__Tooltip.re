let make = currency => {
  Highcharts__Options.(
    Tooltip.make(
      ~backgroundColor="transparent",
      ~borderWidth=0,
      ~padding=14,
      ~distance=20,
      ~headerFormat="<strong>{point.key}</strong>",
      ~pointFormatter=
        [@bs.this] this => {
          let value =
            Numeral.format(Numeral.make(`Int(this##y)), ~format="0.0a", ())
            |> Js.String.toUpperCase;

          (this##series##name |> Js.String.toUpperCase)
          ++ ": "
          ++ (
            currency
            |> Utils.map(currency =>
                 Currency.addCurrencySymbol(currency, value)
               )
            |> Utils.orDefault(value)
          );
        },
      ~shadow=false,
      ~useHTML=true,
      (),
    )
  );
};
