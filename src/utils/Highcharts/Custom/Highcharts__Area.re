let make = (~seriesColor=Colors.malachiteHex, seriesName, seriesData) => {
  Highcharts__Options.(
    Series.area(
      ~color="#" ++ seriesColor,
      ~lineColor="#" ++ seriesColor ++ "AB",
      ~fillColor="#" ++ seriesColor ++ "26",
      ~lineWidth=2,
      ~data=seriesData,
      ~marker=
        Marker.make(
          ~enabled=false,
          ~symbol="circle",
          ~states=
            MarkerState.make(
              ~hover=
                MarkerHoverState.make(
                  ~radius=5,
                  ~lineWidth=3,
                  ~fillColor="#" ++ seriesColor,
                  (),
                ),
              (),
            ),
          (),
        ),
      ~name=seriesName,
      ~states=
        AreaState.make(
          ~hover=AreaHoverState.make(~halo=Halo.make(~size=0, ()), ()),
          (),
        ),
      (),
    )
  );
};
