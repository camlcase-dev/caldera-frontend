let renderOpt: ('a => React.element, option('a)) => React.element =
  (render, item) => {
    switch (item) {
    | Some(item) => render(item)
    | None => React.null
    };
  };

let renderSome = renderOpt(a => a);

let flatMap = (map, data) => Belt.Option.flatMap(data, map);

let map = (map, data) => Belt.Option.map(data, map);

let orDefault = (default, value) =>
  switch (value) {
  | Some(value) => value
  | _ => default
  };

let renderIf = (element: React.element, condition: bool) =>
  condition ? element : React.null;

let filterNone = data => Belt.List.keepMap(data, x => x);

exception DecodeError(string);

let wrapResult = r =>
  switch (r) {
  | r => Belt.Result.Ok(r)
  | exception (Json.Decode.DecodeError(err)) => Belt.Result.Error(err)
  };

let unwrapResult = r =>
  switch (r) {
  | Belt.Result.Ok(v) => v
  | Belt.Result.Error(message) => raise @@ DecodeError(message)
  };

let eventToValue = event => ReactEvent.Form.target(event)##value;

let setRootOverflowHidden = [%raw
  {|
  function() {
    if (typeof document !== 'undefined') {
      const root = document.getElementById("root");
      if (root) root.style.overflowY = "hidden";
    }
  }
|}
];

let setRootOverflowVisible = [%raw
  {|
  function() {
    if (typeof document !== 'undefined') {
      const root = document.getElementById("root");
      if (root) root.style.overflowY = "visible";
    }
  }
|}
];

let nbsp = [%raw {|'\u00a0'|}];
let iInCircle = [%raw {|'\u24D8'|}];
let times = [%raw {|'\u2A09'|}];

[@bs.send.pipe: Dom.element] external blur: unit = "blur";
[@bs.send.pipe: Dom.element] external focus: unit = "focus";

let packageVersion: unit => string = [%bs.raw
  {|
   function () {
     return VERSION;
   }
|}
];

let amplitudeApiKey: unit => string = [%bs.raw
  {|
   function () {
     return AMPLITUDE_API_KEY;
   }
|}
];

let isMaintenanceMode: unit => bool = [%bs.raw
  {|
     function () {
       return MAINTENANCE_MODE === "true";
     }
    |}
];

let dexterUrl = "https://app.dexter.exchange/";
