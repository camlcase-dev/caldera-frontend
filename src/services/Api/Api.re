let isProd: unit => bool = [%bs.raw
  {|
     function () {
       return PROD
     }
    |}
];

let getBasic: unit => string = [%bs.raw
  {|
     function () {
       return AUTH_BASIC;
     }
    |}
];

ErrorReporting.Sentry.init();

let baseUrl =
  isProd()
    ? "https://caldera.dexter.exchange/"
    // : "http://localhost:3000/";
    : "https://dev.caldera.dexter.exchange/";

let requestBaseUrl = (path: string) =>
  Js.Promise.(
    Fetch.fetchWithInit(
      path,
      Fetch.RequestInit.make(
        ~method_=Get,
        ~headers=
          Fetch.HeadersInit.makeWithArray([|
            ("Accept", "application/json"),
            ("Authorization", "Basic " ++ getBasic()),
          |]),
        (),
      ),
    )
    |> then_(Fetch.Response.json)
  );

let requestApi = (~onError=?, path, setJson) => {
  Js.Promise.(
    requestBaseUrl(path)
    |> then_(json => setJson(json) |> resolve)
    |> catch(error => {
         Js.log(error);
         ErrorReporting.Sentry.captureException(
           error,
           ErrorReporting.Sentry.makeIdentifier("Api.requestApi: " ++ path),
         );
         onError |> Utils.map(onError => onError()) |> Utils.orDefault();
         resolve();
       })
  )
  |> ignore;
};

let isEmptyJson = (json: Js.Json.t): bool =>
  json |> Js.Json.stringify === "{}" || json |> Js.Json.stringify === "";

let useRequestWithCache =
    (
      ~path: string,
      ~baseUrl: option(string)=Some(baseUrl),
      ~decoder,
      ~expiresInMilliseconds=120000,
      ~enabled=true,
      ~cacheOnly: bool=false,
      ~fetchOnMount: bool=false,
      ~onError=?,
      (),
    ) => {
  let {cache, setCache} = CacheContext.useContext();
  let currentCache = cache |> Cache.getByPath(path);
  let currentJson = cache |> Cache.getJsonByPath(path);

  let setJson = json => {
    switch (json |> decoder |> Utils.wrapResult) {
    | Belt.Result.Ok(_) =>
      if (!(json |> isEmptyJson)) {
        setCache(
          Cache.upsert({
            expires: Cache.expiresAt(expiresInMilliseconds),
            json,
            path,
          }),
        );
      }
    | Belt.Result.Error(err) =>
      Js.log2("Api.useRequestWithCache setJson failed", err);
      ErrorReporting.Sentry.captureMessage(
        err,
        ErrorReporting.Sentry.makeIdentifier(
          "Api.useRequestWithCache.setJson",
        ),
      );
      onError |> Utils.map(onError => onError()) |> Utils.orDefault();
    };
  };

  let intervalId = ref(None);
  let timeoutId = ref(None);

  let stopPolling = () => {
    timeoutId^
    |> Utils.map(timeoutId => Js.Global.clearTimeout(timeoutId))
    |> Utils.orDefault();
    intervalId^
    |> Utils.map(intervalId => Js.Global.clearInterval(intervalId))
    |> Utils.orDefault();
  };

  React.useEffect2(
    () => {
      if (enabled && !cacheOnly) {
        let path = (baseUrl |> Utils.orDefault("")) ++ path;
        stopPolling();

        let requestDelay =
          fetchOnMount
            ? 0
            : currentCache
              |> Utils.flatMap((cache: Cache.t) =>
                   switch (cache.json |> decoder) {
                   | _ =>
                     Some(
                       MomentRe.diffWithPrecision(
                         cache.expires,
                         MomentRe.momentNow(),
                         `milliseconds,
                         true,
                       )
                       |> int_of_float,
                     )
                   | exception _ => None
                   }
                 )
              |> Utils.orDefault(0);

        timeoutId :=
          Some(
            Js.Global.setTimeout(
              () => {
                requestApi(~onError?, path, setJson);
                intervalId :=
                  Some(
                    Js.Global.setInterval(
                      () => requestApi(path, setJson),
                      expiresInMilliseconds,
                    ),
                  );
              },
              requestDelay,
            ),
          );
      };

      Some(_ => stopPolling());
    },
    (enabled, path),
  );

  React.useMemo2(
    () =>
      enabled
        ? currentJson
          |> Utils.flatMap((json: Js.Json.t) =>
               switch (json |> decoder |> Utils.wrapResult) {
               | Belt.Result.Ok(cache) => Some(cache)
               | Belt.Result.Error(err) =>
                 Js.log2("Api.useRequestWithCache failed", err);
                 ErrorReporting.Sentry.captureMessage(
                   err,
                   ErrorReporting.Sentry.makeIdentifier(
                     "Api.useRequestWithCache",
                   ),
                 );
                 None;
               }
             )
        : None,
    (enabled, currentJson |> Utils.map(Js.Json.stringify)),
  );
};
