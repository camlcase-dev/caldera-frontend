type t('a) = {
  contractId: string,
  value: 'a,
};

let filterByValueCondition =
    (condition: 'a => bool, data: option(array(t('a)))) =>
  switch (data) {
  | Some(data) =>
    Some(data |> Js.Array.filter(data => data.value |> condition))
  | _ => data
  };

let filterByContractId =
    (contract: option(Contract.t), data: option(array(t('a)))) =>
  switch (data, contract) {
  | (Some(data), Some(contract)) =>
    Some(data |> Js.Array.filter(data => data.contractId === contract.id))
  | _ => data
  };

let getSummedValue =
    (
      ~contract: option(Contract.t),
      ~sumFn: array('a) => 'a,
      data: option(array(t('a))),
    )
    : option('a) =>
  data
  |> filterByContractId(contract)
  |> Utils.map(Array.map(data => data.value))
  |> Utils.map(values => values |> sumFn);

let getContractIds = (data: option(array(t('a)))): option(array(string)) =>
  data |> Utils.map(Array.map(data => data.contractId));

let useRawValue =
    (~config: Config.t('a), ~cacheOnly: bool=false, ~enabled: bool=true, ())
    : option(array(t('a))) =>
  Api.useRequestWithCache(
    ~path=config.path ++ config.currentSubpath,
    ~decoder=
      Json.Decode.(
        array(json =>
          {
            contractId: json |> field(config.contractIdFieldName, string),
            value: json |> config.decoder,
          }
        )
      ),
    ~cacheOnly,
    ~enabled,
    (),
  );

let useValueInCurrency =
    (
      ~config: Config.t('a),
      ~contract: option(Contract.t)=None,
      ~cacheOnly: bool=false,
      ~enabled: bool=true,
      (),
    )
    : option(float) =>
  useRawValue(~config, ~cacheOnly, ~enabled, ())
  |> getSummedValue(
       ~contract,
       ~sumFn=Js.Array.reduce((acc, cur) => acc +. cur, 0.),
     )
  |> CurrenciesContext.useValueInCurrency;

let useValueInCurrencyString =
    (
      ~config: Config.t('a),
      ~contract: option(Contract.t)=None,
      ~cacheOnly: bool=false,
      ~enabled: bool=true,
      (),
    )
    : option(string) =>
  useRawValue(~config, ~cacheOnly, ~enabled, ())
  |> getSummedValue(
       ~contract,
       ~sumFn=Js.Array.reduce((acc, cur) => acc +. cur, 0.),
     )
  |> CurrenciesContext.useValueInCurrencyStringRounded;
