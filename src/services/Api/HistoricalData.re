type t = (float, float);

let fromTimestampValue = (timestampValue: TimestampValue.t): t => (
  (fst(timestampValue) |> MomentRe.Moment.toUnix |> float_of_int) *. 1000.,
  snd(timestampValue),
);

let fromTimestampValues =
    (timestampValues: array(TimestampValue.t)): array(t) =>
  timestampValues |> Array.map(fromTimestampValue);

let reshapeData = (timeframe: string, data: array(TimestampValue.t)) => {
  let daysInData = data |> Array.length;

  let missingDays =
    switch (timeframe) {
    | "1w" => Some(7 - daysInData)
    | "1M" => Some(30 - daysInData)
    | "1y" => Some(365 - daysInData)
    | _ => None
    };

  Belt.Option.mapWithDefault(missingDays, data, missingDays => {
    missingDays < 0
      ? data |> Js.Array.sliceFrom(missingDays * (-1))
      : Array.concat([
          Belt.Array.range(1, missingDays)
          |> Array.map(i =>
               (
                 fst(data[0])
                 |> MomentRe.Moment.subtract(
                      ~duration=MomentRe.duration(i |> float_of_int, `days),
                    ),
                 0.,
               )
             )
          |> Belt.Array.reverse,
          data,
        ])
  });
};

let timestampValueDecoder =
    (
      decoder: Json.Decode.decoder(float),
      dateFieldName: string,
      json: Js.Json.t,
    )
    : TimestampValue.t =>
  Json.Decode.(
    json |> field(dateFieldName, Format.decodeMoment),
    json |> decoder,
  );

let useHistoricalPrices = () => {
  let {currentCurrency} = CurrenciesContext.useContext();

  Api.useRequestWithCache(
    ~path=
      "prices/"
      ++ (
        currentCurrency
        |> Utils.map((currentCurrency: Currency.t) => currentCurrency.symbol)
        |> Utils.orDefault("")
      )
      ++ "?timeframe=all",
    ~decoder=
      (json: Js.Json.t): list(TimestampValue.t) =>
        json
        |> Json.Decode.(
             list(json =>
               (
                 json |> field("date", Format.decodeMoment),
                 json |> field("price", Json.Decode.float),
               )
             )
           ),
    ~expiresInMilliseconds=300000,
    ~enabled=
      currentCurrency
      |> Utils.map((currentCurrency: Currency.t) =>
           !(currentCurrency |> Currency.isXTZ)
         )
      |> Utils.orDefault(false),
    (),
  );
};

let useHistoricalData =
    (config: Config.t('a)): option(array(TimestampValue.t)) => {
  let {currentCurrency} = CurrenciesContext.useContext();
  let {currentContract, currentContractIsLoading} =
    ContractsContext.useContext();
  let timeframe = Router.useGraphFilterValue("timeframe");

  let currentValue =
    CurrentData.useValueInCurrency(
      ~config,
      ~contract=currentContract,
      ~cacheOnly=true,
      ~enabled=!currentContractIsLoading,
      (),
    );

  let data =
    Api.useRequestWithCache(
      ~path=
        config.path
        ++ (currentContract |> Contract.getSubpath)
        ++ "?timeframe="
        ++ timeframe,
      ~decoder=
        currentContract |> Belt.Option.isSome
          ? Json.Decode.(
              array(
                timestampValueDecoder(config.decoder, config.dateFieldName),
              )
            )
          : Json.Decode.(
              array(contracts =>
                contracts
                |> array(
                     timestampValueDecoder(
                       config.decoder,
                       config.dateFieldName,
                     ),
                   )
                |> Js.Array.reduce(
                     (acc, cur) =>
                       (cur |> fst, (acc |> snd) +. (cur |> snd)),
                     (MomentRe.momentNow(), 0.),
                   )
              )
            ),
      ~enabled=!currentContractIsLoading,
      (),
    );

  let historicalPrices = useHistoricalPrices();

  React.useMemo4(
    () =>
      switch (currentCurrency) {
      | Some(currentCurrency) =>
        let data =
          currentCurrency |> Currency.isXTZ
            ? data
            : (
              switch (data, historicalPrices) {
              | (Some(data), Some(historicalPrices)) =>
                Some(
                  data
                  |> Array.map(data =>
                       (
                         data |> fst,
                         (data |> snd)
                         *. (
                           historicalPrices
                           |> List.find_opt(price =>
                                MomentRe.Moment.isSame(
                                  fst(price),
                                  fst(data),
                                )
                              )
                           |> Utils.map(price => snd(price))
                           |> Utils.orDefault(0.)
                         ),
                       )
                     ),
                )
              | _ => None
              }
            );

        switch (currentValue, data) {
        | (Some(currentValue), Some(data)) =>
          Some(
            Array.concat([
              data,
              [|
                (
                  MomentRe.momentNow()
                  |> MomentRe.Moment.defaultUtc
                  |> MomentRe.Moment.startOf(`day),
                  currentValue,
                ),
              |],
            ])
            |> reshapeData(timeframe),
          )
        | _ => None
        };
      | _ => None
      },
    (currentCurrency, data, historicalPrices, timeframe),
  );
};

let getNearestFirstDayOfWeek = (date: MomentRe.Moment.t): MomentRe.Moment.t =>
  date
  |> MomentRe.Moment.subtract(
       ~duration=
         MomentRe.duration(
           (date |> MomentRe.Moment.day) - 1 |> float_of_int,
           `days,
         ),
     );

let getNearestFirstDayOfMonth = (date: MomentRe.Moment.t): MomentRe.Moment.t =>
  date
  |> MomentRe.Moment.subtract(
       ~duration=
         MomentRe.duration(
           (date |> MomentRe.Moment.date) - 1 |> float_of_int,
           `days,
         ),
     );

let aggregateByInterval =
    (interval: string, t: array(TimestampValue.t)): array(TimestampValue.t) => {
  let intervalInDays =
    switch (interval) {
    | "m" => 30.
    | "w" => 7.
    | _ => 1.
    };

  intervalInDays === 1.
    ? t
    : t
      |> Js.Array.reduce(
           (acc, cur) => {
             let elementsLength = acc |> Array.length;
             let shouldAdd =
               elementsLength === 0
               || MomentRe.diff(
                    cur |> fst,
                    acc[elementsLength - 1] |> fst,
                    `days,
                  )
               >= intervalInDays;

             if (shouldAdd) {
               let date =
                 interval === "m"
                   ? getNearestFirstDayOfMonth(cur |> fst)
                   : getNearestFirstDayOfWeek(cur |> fst);

               Array.concat([acc, [|(date, cur |> snd)|]]);
             } else {
               acc
               |> Array.mapi((i, el) => {
                    i === (acc |> Array.length) - 1
                      ? (el |> fst, (el |> snd) +. (cur |> snd)) : el
                  });
             };
           },
           [||],
         );
};
