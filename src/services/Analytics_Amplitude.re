type amplitude;

let initAmplitude: unit => amplitude = [%bs.raw
  {|
    function() {
      const amplitude = require('amplitude-js');
      amplitude.getInstance().init(
        // AMPLITUDE_API_KEY
        "0a17e797fb2b0355e8c95d4188080a12");
      amplitude.getInstance().setVersionName(VERSION);
      return amplitude;
    }
   |}
];

let amplitude = initAmplitude();

let logEvent: (amplitude, string, Js.Json.t) => unit = [%bs.raw
  {|
    function(amplitude, eventType, eventProperties) {
      amplitude.getInstance().logEvent(eventType, eventProperties);
    }
  |}
];

let logRouteVisit = (route: string): unit =>
  logEvent(
    amplitude,
    "VISIT_ROUTE",
    Json.Encode.object_([("route", Json.Encode.string(route))]),
  );
