let decoder = (json: Js.Json.t): list(Currency.t) =>
  json
  |> Json.Decode.(
       field("tezos", internal_ => {
         Currency_Dict.list
         |> List.map((currency: Currency.t) =>
              switch (
                internal_
                |> optional(field(currency.symbol, Json.Decode.float))
              ) {
              | Some(rate) => Some({...currency, rate})
              | None when currency.rate !== 0. => Some(currency)
              | None => None
              }
            )
         |> Utils.filterNone
       })
     );

let useCurrencies = (onError): option(list(Currency.t)) => {
  let path =
    "https://api.coingecko.com/api/v3/simple/price?ids=tezos&vs_currencies="
    ++ (
      Currency_Dict.list
      |> List.map((currency: Currency.t) => currency.symbol)
      |> Array.of_list
      |> Js.Array.joinWith(",")
    );

  Api.useRequestWithCache(
    ~path,
    ~baseUrl=None,
    ~decoder,
    ~fetchOnMount=true,
    ~onError,
    (),
  );
};
