module Sentry = {
  type t;

  let initRaw: string => t = [%bs.raw
    {|
     function(environment) {
       const Sentry = require('@sentry/react');
       const { Integrations } = require("@sentry/tracing");

       Sentry.init({
        dsn: SENTRY_DSN,
        attachStacktrace: true,
        release: VERSION,
        environment,
        integrations: [new Integrations.BrowserTracing()],
        // Set tracesSampleRate to 1.0 to capture 100%
        // of transactions for performance monitoring.

        // We recommend adjusting this value in production
        tracesSampleRate: 0.1,
       });
     }
    |}
  ];

  let init = () => initRaw(Deployment.ofRaw()) |> ignore;

  module ScopeContext = {
    type t;

    module Tags = {
      type t;

      [@bs.obj] external make: (~identifier: string) => t;
    };

    [@bs.obj]
    external make: (~fingerprint: array(string), ~tags: Tags.t) => t;
  };

  let makeIdentifier = (identifier: string) =>
    ScopeContext.make(
      ~fingerprint=[|identifier|],
      ~tags=ScopeContext.Tags.make(~identifier),
    );

  [@bs.module "@sentry/browser"]
  external captureMessage: (string, ScopeContext.t) => unit = "captureMessage";

  [@bs.module "@sentry/browser"]
  external captureException: (Js.Promise.error, ScopeContext.t) => unit =
    "captureException";
};
