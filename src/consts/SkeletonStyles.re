type t = {
  height: Flex.heightType,
  skeletonHeight: Flex.heightType,
  background: Flex.backgroundType,
};

let simpleValueLabel: t = {
  height: `px(17),
  skeletonHeight: `px(12),
  background: Colors.linkWater,
};

let simpleValue: t = {
  height: `px(17),
  skeletonHeight: `px(12),
  background: Colors.blueBell,
};

let mainLabel: t = {
  height: `px(16),
  skeletonHeight: `px(14),
  background: Colors.white,
};

let complexStatisticValue: t = {
  height: `px(35),
  skeletonHeight: `px(24),
  background: Colors.white,
};

let exchangeRate: t = {
  height: `px(16),
  skeletonHeight: `px(12),
  background: Colors.whiteWithOpacity80,
};

let tokensListTitle: t = {
  height: `px(22),
  skeletonHeight: `px(16),
  background: Colors.white,
};
