open Css;

type t = list(Css.rule);

let logoText: t = [
  lineHeight(`px(22)),
  fontSize(`px(16)),
  letterSpacing(`px(5)),
  fontWeight(`semiBold),
  color(Colors.whiteLilac),
];

let simpleValueLabel: t = [
  lineHeight(`px(17)),
  fontSize(`px(12)),
  letterSpacing(`pxFloat(2.25)),
  color(Colors.linkWater),
];

let simpleValue: t = [
  lineHeight(`px(17)),
  fontSize(`px(13)),
  letterSpacing(`pxFloat(0.93)),
  fontWeight(`semiBold),
  color(Colors.blueBell),
];

let listGroupTitle: t = [
  lineHeight(`px(14)),
  fontWeight(`num(600)),
  fontSize(`px(10)),
  letterSpacing(`px(1)),
  color(Colors.ebonyClay),
];

let listOptionSymbol: t = [
  lineHeight(`px(15)),
  fontSize(`px(10)),
  letterSpacing(`pxFloat(1.75)),
  fontWeight(`semiBold),
];

let listOptionLabel: t = [
  lineHeight(`px(15)),
  fontSize(`px(11)),
  letterSpacing(`px(0)),
];

let dropdownButtonText: t = [
  lineHeight(`px(14)),
  fontSize(`px(10)),
  letterSpacing(`px(1)),
  color(Colors.ebonyClay),
];

let mainTitle: t = [
  lineHeight(`px(35)),
  fontSize(`px(24)),
  letterSpacing(`pxFloat(0.5)),
  fontWeight(`semiBold),
  color(Colors.white),
];

let mainLabel: t = [
  lineHeight(`px(16)),
  fontSize(`px(14)),
  letterSpacing(`pxFloat(0.5)),
  color(Colors.white),
];

let complexStatisticValue: t = [
  lineHeight(`px(35)),
  fontSize(`px(24)),
  letterSpacing(`px(0)),
  fontWeight(`bold),
  color(Colors.white),
];

let mainContentTab: t = [
  lineHeight(`px(15)),
  fontSize(`px(13)),
  letterSpacing(`pxFloat(0.5)),
];

let searchTab: t = [
  lineHeight(`px(15)),
  fontSize(`px(11)),
  letterSpacing(`px(2)),
];

let searchInput: t = [
  lineHeight(`px(18)),
  fontSize(`px(13)),
  letterSpacing(`pxFloat(0.54)),
  color(Colors.linkWater),
];

let tokensListTitle: t = [
  lineHeight(`px(22)),
  fontSize(`px(16)),
  letterSpacing(`pxFloat(0.5)),
  fontWeight(`semiBold),
  color(Colors.white),
];

let currenciesDropdownTitle: t = [
  lineHeight(`px(25)),
  fontSize(`px(18)),
  letterSpacing(`px(0)),
  fontWeight(`semiBold),
  color(Colors.black),
];

let currenciesDropdownError: t = [
  lineHeight(`px(15)),
  fontSize(`px(11)),
  color(Colors.tundora),
];

let buttonText: t = [
  whiteSpace(`nowrap),
  lineHeight(`px(15)),
  fontWeight(`bold),
  fontSize(`px(11)),
  letterSpacing(`px(2)),
  color(Colors.linkWater),
];

let exchangeRate: t = [
  lineHeight(`px(16)),
  fontSize(`px(12)),
  letterSpacing(`pxFloat(0.5)),
  color(Colors.whiteWithOpacity80),
  whiteSpace(`nowrap),
];

let textButton: t = [
  fontSize(`px(12)),
  letterSpacing(`pxFloat(0.25)),
  color(Colors.white),
  textDecoration(`underline),
];

let tooltip: t = [fontSize(`px(12)), color(Colors.ebonyClay)];
let message: t = [fontSize(`px(14)), color(Colors.ebonyClay)];

let positionsDropdown: t = [
  fontSize(`px(14)),
  letterSpacing(`pxFloat(0.5)),
  lineHeight(`px(24)),
  color(Colors.white),
];

let allLiquidityPositions: t = [
  fontSize(`px(12)),
  lineHeight(`px(16)),
  color(Colors.white),
];

let accountEmptyListMessage: t = [
  fontSize(`px(12)),
  lineHeight(`px(18)),
  color(Colors.linkWaterWithOpacity75),
  textAlign(`center),
];

let tableTitle: t = [
  fontWeight(`semiBold),
  fontSize(`px(14)),
  lineHeight(`px(20)),
  letterSpacing(`pxFloat(0.5)),
  color(Colors.white),
];

let tableText: t = [
  fontSize(`px(12)),
  lineHeight(`px(16)),
  color(Colors.white),
];

let packageVersion: t = [
  fontSize(`px(11)),
  lineHeight(`px(15)),
  color(Colors.white),
];
