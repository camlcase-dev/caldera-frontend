let intervalFilter: Filter.t = {
  name: "interval",
  options: [
    {
      label: "DAILY",
      value: "d",
      hideFor: [{name: "timeframe", values: ["1y", "all"]}],
      isDefault: false,
    },
    {
      label: "WEEKLY",
      value: "w",
      hideFor: [{name: "timeframe", values: ["1w"]}],
      isDefault: true,
    },
    {
      label: "MONTHLY",
      value: "m",
      hideFor: [{name: "timeframe", values: ["1w", "1M"]}],
      isDefault: false,
    },
  ],
};

let timeframeFilter: Filter.t = {
  name: "timeframe",
  options: [
    {label: "1 WEEK", value: "1w", hideFor: [], isDefault: false},
    {label: "30 DAYS", value: "1M", hideFor: [], isDefault: true},
    {label: "1 YEAR", value: "1y", hideFor: [], isDefault: false},
    {label: "ALL-TIME", value: "all", hideFor: [], isDefault: false},
  ],
};

let list: list(Filter.t) = [intervalFilter, timeframeFilter];
