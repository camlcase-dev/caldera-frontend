let isGraphFilter = (name: string): bool =>
  GraphFilters.list |> Filter.findByName(name) |> Belt.Option.isSome;

let getOptionsByFilterNameAndParams =
    (filterName: string, params: list(Param.t))
    : option(list(Filter_Option.t)) => {
  GraphFilters.list
  |> Filter.findByName(filterName)
  |> Utils.map((filter: Filter.t) =>
       filter.options |> Filter_Option.filterHidden(params)
     );
};

let getDefaultParams = (): list(Param.t) => {
  GraphFilters.list
  |> List.map((filter: Filter.t) =>
       (
         {
           name: filter.name,
           value: filter.options |> Filter_Option.getDefaultValue,
         }: Param.t
       )
     );
};

let getDefaultValueByFilterNameAndParams =
    (filterName: string, params: list(Param.t)) =>
  getOptionsByFilterNameAndParams(filterName, params)
  |> Utils.map(Filter_Option.getDefaultValue)
  |> Utils.orDefault("");

let isOptionAvailable = (param: Param.t, params: list(Param.t)): bool =>
  getOptionsByFilterNameAndParams(param.name, params)
  |> Utils.map((options: list(Filter_Option.t)) =>
       options |> Filter_Option.findByValue(param.value) |> Belt.Option.isSome
     )
  |> Utils.orDefault(false);

let orDefault = (name: string, params: list(Param.t), value: option(string)) => {
  switch (value) {
  | Some(value) when isOptionAvailable({name, value}: Param.t, params) => value
  | _ => getDefaultValueByFilterNameAndParams(name, params)
  };
};
