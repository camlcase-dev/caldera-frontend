[@react.component]
let make = () => {
  let {isMobile} = ResponsiveContext.useDevice();
  let contentType = Router.useContentType();

  let (currentTab, setCurrentTab) =
    React.useState(_ => isMobile ? None : contentType);

  let (filter, setFilter) = React.useState(_ => "");

  let setCurrentTab = React.useCallback(tab => setCurrentTab(_ => tab));

  let onSelect =
    React.useCallback(_ => {
      isMobile ? setCurrentTab(None) : ();
      setFilter(_ => "");
    });

  <Flex flexDirection=`column position=`relative>
    <Box
      background=Colors.fiord
      p={`px(10)}
      pt={`px(16)}
      mb={!isMobile || currentTab |> Belt.Option.isSome ? `px(6) : `zero}>
      <MenuTabs currentTab setCurrentTab />
      <MenuSearch filter setFilter currentTab setCurrentTab />
    </Box>
    <MenuBackButton currentTab setCurrentTab />
    {currentTab
     |> Belt.Option.isSome
     |> Utils.renderIf(
          <MenuList>
            {switch (currentTab) {
             | Some(TokenPairs) => <ContractsList filter onSelect />
             | Some(Accounts) => <AccountsList filter onSelect />
             | _ => React.null
             }}
          </MenuList>,
        )}
  </Flex>;
};
