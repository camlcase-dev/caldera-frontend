[@react.component]
let make =
    (
      ~currentTab: option(ContentType.t),
      ~setCurrentTab: option(ContentType.t) => unit,
    ) => {
  let {isMobile} = ResponsiveContext.useDevice();
  let contentType = Router.useContentType();

  (
    isMobile
    && currentTab
    |> Belt.Option.isNone
    && contentType
    |> Belt.Option.isSome
  )
  |> Utils.renderIf(
       <Flex
         mt={`px(16)} ml={`px(8)} onClick={_ => setCurrentTab(contentType)}>
         <Text textStyle=TextStyles.textButton>
           {{js|← Back to |js}
            ++ (
              switch (contentType) {
              | Some(TokenPairs) => "Token Pairs"
              | Some(Accounts) => "Accounts"
              | _ => ""
              }
            )
            |> React.string}
         </Text>
       </Flex>,
     );
};
