[@react.component]
let make = (~children: React.element) => {
  let {isMobile} = ResponsiveContext.useDevice();

  React.useEffect1(
    () => {
      isMobile ? Utils.setRootOverflowHidden() : ();
      isMobile ? Some(() => Utils.setRootOverflowVisible()) : None;
    },
    [|isMobile|],
  );

  <Flex
    className=Css.(
      style([
        borderTopLeftRadius(`px(16)),
        borderTopRightRadius(`px(16)),
        transform(translateY(isMobile ? `percent(100.) : `zero)),
      ])
    )
    flexDirection=`column
    position=?{isMobile ? Some(`absolute) : None}
    bottom=`zero
    width={`percent(100.)}
    zIndex=2
    background=Colors.oxfordBlue
    height=?{isMobile ? Some(`calc((`sub, `vh(100.), `px(170)))) : None}
    overflowY=`auto
    pt={`px(6)}>
    children
  </Flex>;
};
