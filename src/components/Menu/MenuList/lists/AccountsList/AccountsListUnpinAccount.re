[@react.component]
let make = (~account: Account.t) => {
  let {accounts, unpinAccount} = AccountsContext.useContext();

  let onClick =
    React.useCallback1(
      e => {
        e |> ReactEvent.Synthetic.stopPropagation;
        unpinAccount(account);
      },
      [|accounts|],
    );

  <div onClick>
    <Flex
      className=Css.(style([opacity(0.25), hover([opacity(0.4)])]))
      background=Colors.black
      width={`px(14)}
      height={`px(14)}
      alignItems=`center
      justifyContent=`center
      borderRadius={`px(7)}
      pl={`px(1)}>
      <Text
        color=Colors.fiord
        fontWeight=`bold
        fontSize={`px(10)}
        lineHeight={`px(10)}>
        Utils.times
      </Text>
    </Flex>
  </div>;
};
