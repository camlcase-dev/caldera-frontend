[@react.component]
let make = (~filter: string, ~onSelect: unit => unit) => {
  let {accounts} = AccountsContext.useContext();
  let {isMobile} = ResponsiveContext.useDevice();
  let currentAccount = Router.useCurrentAccount();
  let redirectToRoute = Router.useRedirectToRoute();

  accounts |> List.length === 0
    ? <Flex mt={`px(8)} mx=`auto width={`px(192)}>
        <Text textStyle=TextStyles.accountEmptyListMessage>
          {"View stats for any Tezos address that has provided liquidity to Dexter."
           |> React.string}
        </Text>
      </Flex>
    : accounts
      |> List.filter(account =>
           account
           |> String.lowercase_ascii
           |> Js.String.includes(filter |> String.lowercase_ascii)
         )
      |> List.map(account =>
           <MenuListElement
             key=account
             name={account |> Account.toDisplay}
             isActive={!isMobile && currentAccount === Some(account)}
             onClick={_ => {
               redirectToRoute(Account(account, None));
               onSelect();
             }}
             icon={<AccountsListUnpinAccount account />}
           />
         )
      |> Array.of_list
      |> React.array;
};
