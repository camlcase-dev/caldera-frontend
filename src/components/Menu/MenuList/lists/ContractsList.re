[@react.component]
let make = (~filter: string, ~onSelect: unit => unit) => {
  let {contracts} = ContractsContext.useContext();
  let {isMobile} = ResponsiveContext.useDevice();
  let redirectToRoute = Router.useRedirectToRoute();
  let route = Router.useCurrentRoute();

  switch (contracts) {
  | Some(contracts) =>
    contracts
    |> Contract.filterByText(filter)
    |> Contract.sortByXtzPool
    |> List.map((contract: Contract.t) =>
         <MenuListElement
           key={contract.id}
           name={contract.name}
           isActive={
             switch (route) {
             | Overall(Some(routeTokenPair)) when !isMobile =>
               routeTokenPair === contract.name
             | _ => false
             }
           }
           onClick={_ => {
             redirectToRoute(Overall(Some(contract.name)));
             onSelect();
           }}
         />
       )
    |> Array.of_list
    |> React.array
  | _ =>
    Belt.Array.range(0, 4)
    |> Array.map(i =>
         <MenuListElement
           key={i |> string_of_int}
           name={i |> string_of_int}
           isLoading=true
         />
       )
    |> React.array
  };
};
