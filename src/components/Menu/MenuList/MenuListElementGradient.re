[@react.component]
let make = () =>
  <Flex
    position=`absolute
    borderRadius={`px(12)}
    top=`zero
    left=`zero
    right=`zero
    bottom=`zero
    overflow=`hidden
    className=Css.(
      style([
        backfaceVisibility(`hidden),
        transform(translate3d(`zero, `zero, `zero)),
      ])
    )>
    <Flex
      className=Css.(
        style([opacity(0.6), transforms([translateY(`percent(-50.))])])
      )
      width={`percent(400.)}
      pt={`percent(100.)}
      background={Css.radialGradient([
        (`percent(0.), Colors.eucalyptus),
        (`percent(50.), Colors.fiord),
      ])}
      position=`absolute
      maxWidth=`initial
      left={`percent(-150.)}
      top=`zero
      zIndex=0
    />
  </Flex>;
