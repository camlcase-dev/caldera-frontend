[@react.component]
let make =
    (
      ~isLoading: bool=false,
      ~isActive: bool=false,
      ~onClick: option(ReactEvent.Mouse.t => unit)=?,
      ~name: string,
      ~icon: option(React.element)=?,
    ) =>
  <Box
    ?onClick
    background=Colors.fiord
    mb={`px(12)}
    p={`px(18)}
    px={`px(22)}
    overflowX=`hidden
    overflowY=`hidden>
    {isActive |> Utils.renderIf(<MenuListElementGradient />)}
    <Flex zIndex=1 justifyContent=`spaceBetween full=true alignItems=`center>
      <Skeleton
        isLoading
        skeletonStyle=SkeletonStyles.tokensListTitle
        width={`px(77)}>
        <Text textStyle=TextStyles.tokensListTitle>
          {name |> React.string}
        </Text>
        {icon |> Utils.renderSome}
      </Skeleton>
    </Flex>
  </Box>;
