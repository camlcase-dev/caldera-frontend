[@react.component]
let make =
    (
      ~filter: string,
      ~setFilter: (string => string) => unit,
      ~currentTab: option(ContentType.t),
      ~setCurrentTab: option(ContentType.t) => unit,
    ) => {
  let {isMobile} = ResponsiveContext.useDevice();
  let (inputFocused, setInputFocused) = React.useState(_ => false);
  let displayClearIcon = inputFocused || filter !== "";
  let redirectToRoute = Router.useRedirectToRoute();
  let contentType = Router.useContentType();
  let inputRef = React.useRef(Js.Nullable.null);

  let isAccountsTab = currentTab === Some(Accounts);

  let isInvalid =
    isAccountsTab
    && !Account.isValid(filter)
    && Account.hasRequiredLength(filter);

  React.useEffect2(
    () => {
      if (filter |> Account.isValid) {
        redirectToRoute(Account(filter, None));
        isMobile ? setCurrentTab(None) : setCurrentTab(Some(Accounts));
        setFilter(_ => "");
        Js.Nullable.toOption(inputRef.current)
        |> Utils.map(Utils.blur)
        |> Utils.orDefault();
      };
      None;
    },
    (filter, currentTab),
  );

  <Flex position=`relative>
    <Flex
      className=Css.(
        merge([
          style([
            hover([
              selector("input", [placeholder([color(Colors.linkWater)])]),
            ]),
          ]),
          isInvalid ? style([border(`px(1), `solid, Colors.mandy)]) : "",
        ])
      )
      full=true
      onClick={_ => {
        setCurrentTab(
          switch (currentTab, contentType) {
          | (Some(_), _) => currentTab
          | (_, Some(_)) => contentType
          | _ => Some(ContentType.default)
          },
        );
        Js.Nullable.toOption(inputRef.current)
        |> Utils.map(Utils.focus)
        |> Utils.orDefault();
      }}
      p={isInvalid ? `zero : `px(1)}
      background={
        inputFocused
          ? Colors.pickledBluewood : Colors.pickledBluewoodWithOpacity50
      }
      hoverBackground={
        inputFocused
          ? Colors.pickledBluewood : Colors.pickledBluewoodWithOpacity70
      }
      borderRadius={`px(8)}
      py={`px(11)}
      pl={`px(15)}
      pr={`px(39)}>
      <input
        ref={ReactDOMRe.Ref.domRef(inputRef)}
        className=Css.(
          style([
            background(`none),
            border(`zero, `solid, `transparent),
            width(`percent(100.)),
            placeholder([color(Colors.linkWaterWithOpacity25)]),
            ...TextStyles.searchInput,
          ])
        )
        value=filter
        onFocus={_ => setInputFocused(_ => true)}
        onBlur={_ => setInputFocused(_ => false)}
        onChange={ev => {
          let newFilter = ev |> Utils.eventToValue;

          if (!isAccountsTab || !Account.isTooLong(filter)) {
            setFilter(_ => newFilter);
          };
        }}
        placeholder=?{
          inputFocused
            ? None
            : Some(
                currentTab
                |> Utils.map((currentTab: ContentType.t) =>
                     currentTab === Accounts
                   )
                |> Utils.orDefault(contentType === Some(Accounts))
                  ? "tz1..." : "Search...",
              )
        }
      />
    </Flex>
    <Flex
      className=Css.(
        style([
          opacity(0.19),
          transform(translateY(`percent(-50.))),
          hover([opacity(displayClearIcon ? 0.5 : 0.19)]),
        ])
      )
      onClick=?{displayClearIcon ? Some(_ => setFilter(_ => "")) : None}
      position=`absolute
      right={`px(displayClearIcon ? isInvalid ? 6 : 7 : 10)}
      top={`percent(50.)}>
      <Image
        key={displayClearIcon ? "close" : "search"}
        width={`px(displayClearIcon ? 30 : 23)}
        height={`px(displayClearIcon ? 30 : 23)}
        src={displayClearIcon ? "close.svg" : "search.svg"}
      />
    </Flex>
  </Flex>;
};
