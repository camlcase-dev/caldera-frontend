[@react.component]
let make =
    (
      ~currentTab: option(ContentType.t),
      ~setCurrentTab: option(ContentType.t) => unit,
    ) => {
  let redirectToRoute = Router.useRedirectToRoute();
  let contentType = Router.useContentType();

  <Flex height={`px(21)} mb={`px(12)}>
    <Flex
      flexShrink=0.
      className=Css.(
        style([
          opacity(contentType |> Belt.Option.isSome ? 0.25 : 1.),
          hover([opacity(1.)]),
        ])
      )
      onClick={_ => {
        setCurrentTab(None);
        redirectToRoute(Overall(None));
      }}
      mr={`px(6)}
      px={`px(8)}>
      <Image width={`px(17)} height={`px(21)} src="logo-short.svg" />
    </Flex>
    <MenuTab currentTab setCurrentTab tab=TokenPairs />
    <MenuTab currentTab setCurrentTab tab=Accounts />
  </Flex>;
};
