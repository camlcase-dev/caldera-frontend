[@react.component]
let make =
    (
      ~currentTab: option(ContentType.t),
      ~setCurrentTab: option(ContentType.t) => unit,
      ~tab: ContentType.t,
    ) => {
  let contentType = Router.useContentType();
  let isActive =
    currentTab
    |> Utils.map(currentTab => Some(currentTab))
    |> Utils.orDefault(contentType) === Some(tab);
  let onClick = React.useCallback0(_ => setCurrentTab(Some(tab)));

  <Flex flexShrink=0. onClick mt={`px(1)} mr={`px(8)}>
    <Text
      className={Css.style([Css.width(`px(tab === Accounts ? 64 : 95))])}
      textStyle=TextStyles.searchTab
      color={isActive ? Colors.linkWater : Colors.linkWaterWithOpacity25}
      fontWeight={isActive ? `bold : `normal}
      hoverColor=Colors.linkWater>
      {tab |> ContentType.getTitle |> React.string}
    </Text>
  </Flex>;
};
