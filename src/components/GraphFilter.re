[@react.component]
let make = (~filter: Filter.t) => {
  let {name, options} = filter;
  let {isMobile} = ResponsiveContext.useDevice();
  let redirectToParam = Router.useRedirectToParam();
  let params = Router.useParams();

  let filteredOptions = options |> Filter_Option.filterHidden(params);
  let currentOption =
    filteredOptions |> Filter_Option.getCurrentOption(params, name);

  let (listVisible, setListVisible) = React.useState(_ => false);

  let toggleListVisible =
    React.useCallback0(_ => setListVisible(listVisible => !listVisible));

  let onOptionClick = value => {
    redirectToParam({name, value});
    toggleListVisible();
  };

  <Flex ml={`px(12)} position=`relative>
    <Dropdown
      button={
        <DropdownButton
          background={listVisible ? Colors.blueBell : Colors.periwinkle}
          disabled={filteredOptions |> List.length <= 1}
          hoverBackground=?{isMobile ? None : Some(Colors.blueBell)}
          onClick=toggleListVisible
          width={`px(81)}>
          {currentOption.label |> React.string}
        </DropdownButton>
      }
      bottom={`px(-8)}
      list={
        !isMobile
        |> Utils.renderIf(
             <SimpleList
               onOptionClick
               options={filteredOptions |> Filter_Option.toSimpleListOptions}
               value={currentOption.value}
             />,
           )
      }
      listVisible
      toggleListVisible
    />
    {isMobile
     |> Utils.renderIf(
          <SimpleListSelect
            onBlur={_ => toggleListVisible()}
            onOptionClick
            options={filteredOptions |> Filter_Option.toSimpleListOptions}
            value={currentOption.value}
          />,
        )}
  </Flex>;
};
