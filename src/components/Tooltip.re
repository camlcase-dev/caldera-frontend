[@react.component]
let make =
    (
      ~children: React.element,
      ~shift: int=0,
      ~text: React.element,
      ~width: option(Flex.widthType)=?,
    ) => {
  <Flex
    className=Css.(
      style([
        cursor(`default),
        hover([
          selector(".popover", [opacity(1.), visibility(`visible)]),
        ]),
      ])
    )
    inlineFlex=true
    position=`relative>
    <Flex
      className=Css.(
        merge([
          style([
            transform(
              translate(
                `calc((`add, `percent(-50.), `px(shift))),
                `percent(-100.),
              ),
            ),
            opacity(0.),
            visibility(`hidden),
            zIndex(2),
          ]),
          "popover",
        ])
      )
      left={`percent(50.)}
      position=`absolute
      top=`zero
      maxWidth=Css.initial
      pb={`px(12)}>
      <Flex
        background=Colors.white
        borderRadius={`px(8)}
        p={`px(12)}
        ?width
        className=Css.(
          style([
            boxShadow(
              Shadow.box(
                ~y=px(2),
                ~blur=px(10),
                rgba(0, 0, 0, `num(0.25)),
              ),
            ),
            before([
              position(`absolute),
              left(pct(50.)),
              bottom(px(13)),
              contentRule(`text("")),
              transform(translate(pct(-50.), pct(100.))),
              border(px(10), `solid, `transparent),
              borderTopColor(Colors.white),
              borderBottomStyle(`none),
            ]),
          ])
        )>
        text
      </Flex>
    </Flex>
    children
  </Flex>;
};
