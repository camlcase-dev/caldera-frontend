[@react.component]
let make =
    (
      ~isActive: bool=false,
      ~icon: option(React.element)=?,
      ~onClick: option(_ => unit)=?,
      ~text: option(string)=?,
    ) =>
  <Flex
    justifyContent=`center
    alignItems=`center
    px={`px(icon |> Belt.Option.isSome ? 17 : 20)}
    height={`px(36)}
    borderRadius={`px(18)}
    background=?{isActive ? Some(Colors.malachiteWithOpacity50) : None}
    className=Css.(
      style([
        border(`px(2), `solid, isActive ? `transparent : Colors.malachite),
        media(
          "(min-width: 992px)",
          [hover([background(Colors.malachite)])],
        ),
      ])
    )
    ?onClick>
    {icon |> Utils.renderSome}
    {text
     |> Utils.renderOpt(text =>
          <>
            {icon |> Utils.renderOpt(_ => <Flex width={`px(8)} />)}
            <Text
              textStyle=TextStyles.buttonText
              fontStyle=?{isActive ? Some(`italic) : None}>
              {text |> React.string}
            </Text>
          </>
        )}
  </Flex>;
