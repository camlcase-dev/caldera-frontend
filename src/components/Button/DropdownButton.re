[@react.component]
let make =
    (
      ~background: Flex.backgroundType,
      ~children: React.element,
      ~color: option(Css.Types.Color.t)=?,
      ~disabled: bool=false,
      ~hoverBackground: option(Flex.backgroundType)=?,
      ~onClick: _ => unit,
      ~textDecoration: option(Text.textDecorationType)=?,
      ~width: Flex.widthType,
    ) =>
  <Flex
    disabled
    onClick
    background
    ?hoverBackground
    alignItems=`center
    justifyContent=`center
    py={`px(8)}
    width
    borderRadius={`px(14)}>
    <Text textStyle=TextStyles.dropdownButtonText ?color ?textDecoration>
      children
    </Text>
    <Flex
      ml={`px(4)}
      className={Css.style([
        Css.borderLeft(`px(4), `solid, `transparent),
        Css.borderRight(`px(4), `solid, `transparent),
        Css.borderTop(
          `px(4),
          `solid,
          color |> Utils.orDefault(Colors.ebonyClay),
        ),
      ])}
    />
  </Flex>;
