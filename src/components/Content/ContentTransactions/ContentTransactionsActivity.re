[@react.component]
let make =
    (
      ~index: int,
      ~title: string,
      ~tokenAmount1: string,
      ~tokenAmount2: option(string)=?,
      ~totalValue: string,
      ~timestamp: string,
    ) => {
  let {isTablet} = ResponsiveContext.useDevice();
  let isEven = index mod 2 === 0;
  let txHash = "onnocJEsmhiRqKv7hmcZBzzRadDfMk7oS7LWLzQCY13oPkCzCzy";

  <>
    <ContentTransactionsRow
      background=?{isTablet === isEven ? Some(Colors.fiord) : None}
      col1={<Text fontWeight=`semiBold> {title |> React.string} </Text>}
      col2={tokenAmount1 |> React.string}
      col3=?{
        (
          switch (tokenAmount2, isTablet) {
          | (Some(_), _) => tokenAmount2
          | (None, false) => Some("N/A")
          | _ => None
          }
        )
        |> Utils.map(tokenAmount2 => {tokenAmount2 |> React.string})
      }
      col4={totalValue |> React.string}
      col5={
        <a href={j|http://tezblock.io/transaction/$txHash|j} target="_blank">
          <Text textStyle=TextStyles.tableText textDecoration=`underline>
            {timestamp |> React.string}
          </Text>
        </a>
      }
    />
    <Flex mb={isTablet ? `zero : `px(8)} />
  </>;
};
