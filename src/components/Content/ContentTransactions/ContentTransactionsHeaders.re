[@react.component]
let make = () =>
  <ContentTransactionsRow
    col1={
      <Text color=Colors.whiteWithOpacity75>
        {"Activity" |> React.string}
      </Text>
    }
    col2={
      <Text color=Colors.whiteWithOpacity75>
        {"Token amount" |> React.string}
      </Text>
    }
    col3={
      <Text color=Colors.whiteWithOpacity75>
        {"Token amount" |> React.string}
      </Text>
    }
    col4={
      <Text color=Colors.whiteWithOpacity75>
        {"Total value" |> React.string}
      </Text>
    }
    col5={
      <Text color=Colors.whiteWithOpacity75>
        {"Timestamp" |> React.string}
      </Text>
    }
  />;
