[@react.component]
let make = (~account: Account.t) => {
  let {isMobile, isTablet} = ResponsiveContext.useDevice();
  Js.log(account);

  <Box
    background={
      isMobile ? Colors.pickledBluewoodWithOpacity42 : Colors.oxfordBlue
    }
    pt={`px(18)}
    px={`px(isTablet ? 8 : 20)}
    pb={`px(30)}
    mb={`px(12)}>
    <Flex mx={isTablet ? `px(4) : `zero} mb={`px(isTablet ? 8 : 4)}>
      <Text textStyle=TextStyles.tableTitle>
        {"Transactions" |> React.string}
      </Text>
    </Flex>
    {!isTablet |> Utils.renderIf(<ContentTransactionsHeaders />)}
    <ContentTransactionsActivity
      index=0
      title="Remove Liquidity"
      tokenAmount1="150.783812 XTZ"
      tokenAmount2="0.07829101 tzBTC"
      totalValue="$602.41"
      timestamp="4 hours ago"
    />
    <ContentTransactionsActivity
      index=1
      title="Remove Liquidity"
      tokenAmount1="150.783812 XTZ"
      tokenAmount2="0.07829101 tzBTC"
      totalValue="$602.41"
      timestamp="4 hours ago"
    />
    <ContentTransactionsActivity
      index=2
      title="Baking Rewards"
      tokenAmount1="1.194325 XTZ"
      totalValue="$3.41"
      timestamp="4 days ago"
    />
    <ContentTransactionsActivity
      index=3
      title="Add Liquidity"
      tokenAmount1="1,500 XTZ"
      tokenAmount2="0.52874141 tzBTC"
      totalValue="$602.41"
      timestamp="36 days ago"
    />
    <ContentTransactionsPagination />
  </Box>;
};
