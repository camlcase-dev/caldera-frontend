[@react.component]
let make =
    (
      ~background: option(Flex.backgroundType)=?,
      ~col1: React.element,
      ~col2: React.element,
      ~col3: option(React.element)=?,
      ~col4: React.element,
      ~col5: React.element,
      ~mb: option(Flex.marginType)=?,
    ) => {
  let {isTablet} = ResponsiveContext.useDevice();

  <Flex
    ?background
    ?mb
    mx={`px(4)}
    px={`px(isTablet ? 12 : 16)}
    alignItems=`center
    height={`px(isTablet ? 52 : 32)}
    flexGrow=1.>
    {isTablet
       ? <>
           <Flex flexDirection=`column flexGrow=1.>
             <Text textStyle=TextStyles.tableText> col1 </Text>
             <Flex>
               <Text textStyle=TextStyles.tableText>
                 col2
                 {col3
                  |> Utils.renderOpt(col3 => <> {"/" |> React.string} col3 </>)}
               </Text>
             </Flex>
           </Flex>
           <Flex flexDirection=`column alignItems=`flexEnd>
             <Text textStyle=TextStyles.tableText> col5 </Text>
             <Text textStyle=TextStyles.tableText> col4 </Text>
           </Flex>
         </>
       : <>
           <Flex width={`percent(20.)}>
             <Text textStyle=TextStyles.tableText> col1 </Text>
           </Flex>
           <Flex width={`percent(22.)} justifyContent=`flexEnd>
             <Text textStyle=TextStyles.tableText> col2 </Text>
           </Flex>
           <Flex width={`percent(22.)} justifyContent=`flexEnd>
             <Text textStyle=TextStyles.tableText>
               {col3 |> Utils.renderSome}
             </Text>
           </Flex>
           <Flex width={`percent(18.)} justifyContent=`flexEnd>
             <Text textStyle=TextStyles.tableText> col4 </Text>
           </Flex>
           <Flex width={`percent(18.)} justifyContent=`flexEnd>
             <Text textStyle=TextStyles.tableText> col5 </Text>
           </Flex>
         </>}
  </Flex>;
};
