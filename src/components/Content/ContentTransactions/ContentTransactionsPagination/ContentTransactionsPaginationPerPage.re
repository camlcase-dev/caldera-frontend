let options: list(SimpleList.t) = [
  {label: "10", value: "10"},
  {label: "25", value: "25"},
  {label: "50", value: "50"},
];

[@react.component]
let make = (~perPage: int, ~setPerPage: (int => int) => unit) => {
  let {isMobile} = ResponsiveContext.useDevice();
  let (listVisible, setListVisible) = React.useState(_ => false);

  let toggleListVisible =
    React.useCallback0(_ => setListVisible(listVisible => !listVisible));

  let onOptionClick =
    React.useCallback0(value => {
      setPerPage(_ => value |> int_of_string);
      toggleListVisible();
    });

  let currentOption =
    React.useMemo1(
      () =>
        options
        |> List.find((option: SimpleList.t) =>
             option.value |> int_of_string === perPage
           ),
      [|perPage|],
    );

  <Flex>
    <Text textStyle=TextStyles.tableText color=Colors.whiteWithOpacity75>
      {"Show per page:" |> React.string}
    </Text>
    <Flex position=`relative>
      <Dropdown
        button={
          <Flex
            ml={`px(1)}
            pl={`px(3)}
            pr={`px(5)}
            onClick={_ => toggleListVisible()}
            hoverBackground=Colors.pickledBluewood
            background=?{listVisible ? Some(Colors.pickledBluewood) : None}
            borderRadius={`px(4)}
            alignItems=`center>
            <Text textStyle=TextStyles.tableText>
              {currentOption.label |> React.string}
            </Text>
            <Flex ml={`px(5)}>
              <Image width={`px(5)} rotate=(-90.) src="arrow.svg" />
            </Flex>
          </Flex>
        }
        list={
          !isMobile
          |> Utils.renderIf(
               <SimpleList
                 onOptionClick
                 options
                 px={`px(16)}
                 value={currentOption.value}
                 width=`initial
               />,
             )
        }
        listPosition=Top
        listVisible
        right={`px(-8)}
        toggleListVisible
        top={`px(-8)}
      />
      {isMobile
       |> Utils.renderIf(
            <SimpleListSelect
              value={currentOption.value}
              onBlur={_ => toggleListVisible()}
              onOptionClick
              options
            />,
          )}
    </Flex>
  </Flex>;
};
