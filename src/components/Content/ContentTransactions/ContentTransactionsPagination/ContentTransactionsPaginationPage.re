[@react.component]
let make = (~page: int, ~setPage: (int => int) => unit, ~maxPage: int) => {
  let isFirstPage = page === 0;
  let isLastPAge = page === maxPage;

  <Flex my={`px(-4)} alignItems=`center>
    <Flex p={`px(4)} disabled=isFirstPage onClick={_ => setPage(_ => 0)}>
      <Image height={`px(10)} src="arrowEnd.svg" />
    </Flex>
    <Flex
      p={`px(4)} disabled=isFirstPage onClick={_ => setPage(_ => page - 1)}>
      <Image height={`px(10)} src="arrow.svg" />
    </Flex>
    <Flex mx={`px(8)}>
      <Text textStyle=TextStyles.tableText color=Colors.whiteWithOpacity75>
        {"Page "
         ++ (page + 1 |> string_of_int)
         ++ " of "
         ++ (maxPage + 1 |> string_of_int)
         |> React.string}
      </Text>
    </Flex>
    <Flex
      p={`px(4)} disabled=isLastPAge onClick={_ => setPage(_ => page + 1)}>
      <Image height={`px(10)} rotate=180. src="arrow.svg" />
    </Flex>
    <Flex p={`px(4)} disabled=isLastPAge onClick={_ => setPage(_ => maxPage)}>
      <Image height={`px(10)} rotate=180. src="arrowEnd.svg" />
    </Flex>
  </Flex>;
};
