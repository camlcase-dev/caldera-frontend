[@react.component]
let make = () => {
  let {isTablet} = ResponsiveContext.useDevice();
  let maxPage = 7;
  let (page, setPage) = React.useState(_ => 0);
  let (perPage, setPerPage) = React.useState(_ => 10);

  <Flex
    justifyContent=`spaceBetween
    mt={isTablet ? `px(16) : `px(8)}
    mx={`px(12)}>
    <ContentTransactionsPaginationPerPage perPage setPerPage />
    <ContentTransactionsPaginationPage page setPage maxPage />
  </Flex>;
};
