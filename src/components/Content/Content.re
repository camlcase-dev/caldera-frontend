[@react.component]
let make = () => {
  let currentRoute = Router.useCurrentRoute();
  let {isMobile} = ResponsiveContext.useDevice();

  <Flex flexDirection=`column>
    <Box
      pt={`px(14)}
      pb={isMobile ? `zero : `px(20)}
      px={isMobile ? `zero : `px(32)}
      background=?{isMobile ? None : Some(Colors.fiord)}
      overflowY=`visible>
      {!isMobile |> Utils.renderIf(<ContentGradient />)}
      <ContentHeader />
      <ContentStats />
      {switch (currentRoute) {
       | Account(_) => React.null
       | _ => <Subcontent />
       }}
      {switch (currentRoute) {
       //  | Account(account, _) => <ContentTransactions account />
       | _ => React.null
       }}
    </Box>
    <Flex
      mt={isMobile ? `zero : `px(8)} px={`px(16)} justifyContent=`flexEnd>
      <Text textStyle=TextStyles.packageVersion>
        {React.string("v" ++ Utils.packageVersion())}
      </Text>
    </Flex>
  </Flex>;
};
