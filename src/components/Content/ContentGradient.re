[@react.component]
let make = () => {
  <Flex
    className=Css.(
      style([
        backfaceVisibility(`hidden),
        transform(translate3d(`zero, `zero, `zero)),
      ])
    )
    position=`absolute
    top=`zero
    left=`zero
    right=`zero
    bottom=`zero
    overflow=`hidden
    borderRadius={`px(12)}>
    <Flex
      className=Css.(
        style([
          transforms([
            translateX(`percent(-14.)),
            translateY(`percent(-64.)),
          ]),
        ])
      )
      width={`percent(200.)}
      pt={`percent(40.)}
      background={Css.radialGradient([
        (`percent(0.), Colors.eucalyptus),
        (`percent(50.), Colors.fiord),
      ])}
      position=`absolute
      maxWidth=`initial
      top=`zero
      left=`zero
      zIndex=0
    />
  </Flex>;
};
