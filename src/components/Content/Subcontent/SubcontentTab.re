[@react.component]
let make = (~tab: SubcontentType.t) => {
  let redirectToParam = Router.useRedirectToParam();
  let subcontentType = Router.useSubcontentType();
  let isActive = tab === subcontentType;

  let onClick =
    isActive
      ? None : Some(_ => redirectToParam(tab |> SubcontentType.toParam));

  <Flex
    className=Css.(
      style(
        isActive ? [borderBottom(`px(3), solid, Colors.malachite)] : [],
      )
    )
    width={`px(80)}
    alignItems=`flexStart
    justifyContent=`center
    flexShrink=0.
    height={`px(28)}
    mr={`px(8)}
    ?onClick>
    <Text
      textStyle=TextStyles.mainContentTab
      fontWeight={isActive ? `semiBold : `normal}
      color={isActive ? Colors.whiteLilac : Colors.whiteLilacWithOpacity}
      hoverColor=Colors.whiteLilac>
      {tab |> SubcontentType.getTitle |> React.string}
    </Text>
  </Flex>;
};
