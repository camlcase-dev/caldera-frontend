[@react.component]
let make = () => {
  let {isXXSmall, isMobile} = ResponsiveContext.useDevice();
  let currentRoute = Router.useCurrentRoute();
  let subcontentType = Router.useSubcontentType();

  <Box
    background=Colors.pickledBluewood
    pt={`px(18)}
    px={`px(22)}
    pb=`zero
    mb={`px(isMobile ? 12 : 16)}>
    <Flex alignItems=`center flexWrap={isXXSmall ? `wrap : `nowrap}>
      <Flex>
        <SubcontentTab tab=Liquidity />
        {switch (currentRoute) {
         | Account(_) => <SubcontentTab tab=Rewards />
         | _ => <SubcontentTab tab=Volume />
         }}
      </Flex>
      <Flex
        flexGrow=1.
        width={isXXSmall ? `percent(100.) : `initial}
        justifyContent=`flexEnd
        mt={isXXSmall ? `px(16) : `zero}>
        {subcontentType === Volume
         |> Utils.renderIf(<GraphFilter filter=GraphFilters.intervalFilter />)}
        <GraphFilter filter=GraphFilters.timeframeFilter />
      </Flex>
    </Flex>
    {switch (subcontentType) {
     | Rewards => <GraphRewards />
     | Liquidity => <GraphLiquidity />
     | Volume => <GraphVolume />
     }}
  </Box>;
};
