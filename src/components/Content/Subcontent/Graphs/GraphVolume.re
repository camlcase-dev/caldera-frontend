[@react.component]
let make = () => {
  let graphData = HistoricalData.useHistoricalData(Volume.config);

  let interval = Router.useGraphFilterValue("interval");

  let (aggregatedData, setAggregatedData) = React.useState(_ => None);

  let timeoutId = ref(None);

  React.useEffect2(
    () => {
      if (timeoutId^ |> Belt.Option.isNone) {
        setAggregatedData(_ => None);

        timeoutId :=
          Some(
            Js.Global.setTimeout(
              () =>
                setAggregatedData(_ => {
                  graphData
                  |> Utils.map(HistoricalData.aggregateByInterval(interval))
                }),
              200,
            ),
          );
      };

      Some(
        () => {
          timeoutId^
          |> Utils.map(timeoutId => Js.Global.clearTimeout(timeoutId))
          |> Utils.orDefault()
        },
      );
    },
    (interval, graphData),
  );

  React.useMemo1(
    () => {
      <Graph
        graphSeries=?{
          aggregatedData
          |> Utils.map(aggregatedData =>
               [|
                 Highcharts.Column.make(
                   "Total volume",
                   aggregatedData |> HistoricalData.fromTimestampValues,
                 ),
               |]
             )
        }
      />
    },
    [|aggregatedData|],
  );
};
