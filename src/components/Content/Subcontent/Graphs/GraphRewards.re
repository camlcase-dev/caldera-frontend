[@react.component]
let make = () => {
  let graphData = HistoricalData.useHistoricalData(Liquidity.config);
  let graphData1 = HistoricalData.useHistoricalData(Volume.config);

  let (aggregatedData, setAggregatedData) = React.useState(_ => None);

  let timeoutId = ref(None);

  React.useEffect1(
    () => {
      if (timeoutId^ |> Belt.Option.isNone) {
        setAggregatedData(_ => None);

        switch (graphData, graphData1) {
        | (Some(graphData), Some(graphData1)) =>
          timeoutId :=
            Some(
              Js.Global.setTimeout(
                () =>
                  setAggregatedData(_ =>
                    Some(
                      graphData
                      |> Array.map(g =>
                           (
                             fst(g),
                             snd(g)
                             +. (
                               graphData1
                               |> Js.Array.find(g1 =>
                                    MomentRe.Moment.isSame(fst(g), fst(g1))
                                  )
                               |> Utils.map(g1 => snd(g1))
                               |> Utils.orDefault(0.)
                             ),
                           )
                         ),
                    )
                  ),
                200,
              ),
            )
        | _ => ()
        };
      };

      Some(
        () => {
          timeoutId^ |> Utils.map(Js.Global.clearTimeout) |> Utils.orDefault()
        },
      );
    },
    [|graphData, graphData1|],
  );

  React.useMemo1(
    () => {
      <Graph
        crosshair=true
        graphSeries=?{
          switch (aggregatedData, graphData, graphData1) {
          | (Some(aggregatedData), Some(graphData), Some(graphData1)) =>
            Some([|
              Highcharts.Area.make(
                "Total",
                aggregatedData |> HistoricalData.fromTimestampValues,
              ),
              Highcharts.Area.make(
                ~seriesColor=Colors.royalBlueHex,
                "Liquidity rewards",
                graphData |> HistoricalData.fromTimestampValues,
              ),
              Highcharts.Area.make(
                ~seriesColor=Colors.heliotropeHex,
                "Baking rewards",
                graphData1 |> HistoricalData.fromTimestampValues,
              ),
            |])
          | _ => None
          }
        }
        tickInterval=?{
          aggregatedData
          |> Utils.flatMap(a =>
               a |> Array.length > 21 && a |> Array.length < 30
                 ? Some(7 * 24 * 3600 * 1000) : None
             )
        }
        withLegend=true
      />
    },
    [|aggregatedData|],
  );
};
