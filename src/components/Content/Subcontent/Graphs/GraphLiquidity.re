[@react.component]
let make = () => {
  let graphData = HistoricalData.useHistoricalData(Liquidity.config);

  let (aggregatedData, setAggregatedData) = React.useState(_ => None);

  let timeoutId = ref(None);

  React.useEffect1(
    () => {
      if (timeoutId^ |> Belt.Option.isNone) {
        setAggregatedData(_ => None);

        timeoutId :=
          Some(
            Js.Global.setTimeout(
              () => setAggregatedData(_ => graphData),
              200,
            ),
          );
      };

      Some(
        () => {
          timeoutId^ |> Utils.map(Js.Global.clearTimeout) |> Utils.orDefault()
        },
      );
    },
    [|graphData|],
  );

  React.useMemo1(
    () => {
      <Graph
        crosshair=true
        graphSeries=?{
          aggregatedData
          |> Utils.map(aggregatedData =>
               [|
                 Highcharts.Area.make(
                   "Total liquidity",
                   aggregatedData |> HistoricalData.fromTimestampValues,
                 ),
               |]
             )
        }
        tickInterval=?{
          aggregatedData
          |> Utils.flatMap(a =>
               a |> Array.length > 21 && a |> Array.length < 30
                 ? Some(7 * 24 * 3600 * 1000) : None
             )
        }
      />
    },
    [|aggregatedData|],
  );
};
