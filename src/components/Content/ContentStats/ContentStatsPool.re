[@react.component]
let make = (~children: React.element, ~label: string, ~isLoading: bool) => {
  let {isMobile} = ResponsiveContext.useDevice();

  <Flex
    className=Css.(
      style([
        borderBottomLeftRadius(`px(12)),
        borderBottomRightRadius(`px(12)),
      ])
    )
    background=Colors.pickledBluewood
    py={`px(10)}
    px={`px(isMobile ? 24 : 28)}>
    <Image
      className=Css.(style([opacity(0.67), marginTop(`px(3))]))
      width={`px(19)}
      height={`px(12)}
      src="pool.svg"
    />
    <Flex ml={`px(8)} alignItems=`center flexWrap=`wrap>
      <Skeleton
        isLoading
        skeletonStyle=SkeletonStyles.simpleValueLabel
        background=Colors.cadetBlue>
        <Text textStyle=TextStyles.simpleValueLabel color=Colors.cadetBlue>
          {label |> React.string}
        </Text>
      </Skeleton>
      children
    </Flex>
  </Flex>;
};
