[@react.component]
let make =
    (
      ~accountLiquidityRewards:
         option(array(CurrentData.t(AccountLiquidityRewards.t))),
    ) => {
  let {currentContract} = ContractsContext.useContext();
  let tokenPair = Router.useTokenPair();

  let (tokenReward, tokenRewardInXtz, xtzReward, totalReward) =
    accountLiquidityRewards
    |> CurrentData.getSummedValue(
         ~contract=currentContract,
         ~sumFn=AccountLiquidityRewards.sumFn,
       )
    |> Utils.map((accountLiquidityRewards: AccountLiquidityRewards.t) =>
         (
           Some(accountLiquidityRewards.tokenReward),
           Some(accountLiquidityRewards.tokenRewardInXtz),
           Some(accountLiquidityRewards.xtzReward),
           Some(accountLiquidityRewards.totalReward),
         )
       )
    |> Utils.orDefault((None, None, None, None));

  let totalRewardInCurrency =
    totalReward |> CurrenciesContext.useValueInCurrencyStringRounded;

  let tokenRewardInCurrency =
    tokenRewardInXtz |> CurrenciesContext.useValueInCurrencyString;

  let xtzRewardInCurrency =
    xtzReward |> CurrenciesContext.useValueInCurrencyString;

  let xtzReward = Format.tokenToDisplay(xtzReward, "XTZ");

  let tokenReward =
    Format.tokenToDisplay(tokenReward, tokenPair |> TokenPair.getToken);

  <ContentStatsValue
    label="Liquidity Rewards"
    tooltipText="These are the lifetime liquidity rewards for providing liquidity to Dexter (in terms of local currency)"
    value=totalRewardInCurrency>
    {tokenPair
     |> Utils.renderOpt(_ =>
          <>
            <ContentStatsSubvalue
              currencyValue=?{
                xtzRewardInCurrency |> Utils.map(Format.addBracketsToDisplay)
              }
              value=xtzReward
            />
            <ContentStatsSubvalue
              currencyValue=?{
                tokenRewardInCurrency
                |> Utils.map(Format.addBracketsToDisplay)
              }
              value=tokenReward
              valueSkeletonWidth={`px(110)}
            />
          </>
        )}
  </ContentStatsValue>;
};
