[@react.component]
let make =
    (~disabled: bool, ~listVisible: bool, ~toggleListVisible: _ => unit) => {
  let tokenPair = Router.useTokenPair();

  <Flex
    className=?{
      listVisible
        ? Some(
            Css.(
              style([
                borderBottomLeftRadius(`zero),
                borderBottomRightRadius(`zero),
              ])
            ),
          )
        : None
    }
    alignItems=`center
    background=Colors.pickledBluewood
    borderRadius={`px(4)}
    disabled
    flexGrow=1.
    justifyContent=`spaceBetween
    height={`px(45)}
    px={`px(16)}
    onClick=toggleListVisible
    zIndex=?{listVisible ? Some(3) : None}>
    <Text
      textStyle=TextStyles.positionsDropdown ellipsis=true whiteSpace=`nowrap>
      {tokenPair |> Utils.orDefault("All liquidity positions") |> React.string}
    </Text>
    <Flex ml={`px(4)} />
    <Image
      className=Css.(
        style([filter([`brightness(1000.), `grayscale(100.)])])
      )
      rotate=(-90.)
      src="arrow.svg"
      height={`px(14)}
    />
  </Flex>;
};
