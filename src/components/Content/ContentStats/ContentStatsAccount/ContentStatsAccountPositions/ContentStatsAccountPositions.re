[@react.component]
let make =
    (
      ~currentPositions: option(array(string)),
      ~pastPositions: option(array(string)),
    ) => {
  let (listVisible, setListVisible) = React.useState(_ => false);

  let toggleListVisible =
    React.useCallback0(_ => setListVisible(listVisible => !listVisible));

  let disabled =
    switch (currentPositions, pastPositions) {
    | (Some(currentPositions), Some(pastPositions)) =>
      Array.concat([currentPositions, pastPositions]) |> Array.length === 0
    | _ => false
    };

  <Dropdown
    button={
      <ContentStatsAccountPositionsButton
        disabled
        listVisible
        toggleListVisible
      />
    }
    background=Colors.blackWithOpacity33
    bottom=`zero
    list={
      <ContentStatsAccountPositionsList
        currentPositions
        pastPositions
        toggleListVisible
      />
    }
    listVisible={listVisible && !disabled}
    toggleListVisible
  />;
};
