[@react.component]
let make =
    (
      ~currentPositions: option(array(string)),
      ~pastPositions: option(array(string)),
      ~toggleListVisible: unit => unit,
    ) => {
  let {isMobile} = ResponsiveContext.useDevice();
  let {currentContract, contracts} = ContractsContext.useContext();
  let account = Router.useCurrentAccount();
  let redirectToRoute = Router.useRedirectToRoute();

  let (value, setValue) = React.useState(_ => "");

  let onSelect =
    React.useCallback1(
      (contract: option(Contract.t)) => {
        account
        |> Utils.map(account => {
             redirectToRoute(
               Account(
                 account,
                 contract
                 |> Utils.map((contract: Contract.t) => contract.name),
               ),
             );
             toggleListVisible();
           })
        |> Utils.orDefault()
      },
      [|account|],
    );

  <Flex
    className=Css.(
      style([
        borderBottomLeftRadius(`px(4)),
        borderBottomRightRadius(`px(4)),
      ])
    )
    flexDirection=`column
    background=Colors.fiord
    full=true
    px={`px(16)}>
    {switch (contracts) {
     | Some(contracts) =>
       let contracts = contracts |> Contract.filterByText(value);
       let listGroups = [
         (
           "Current positions",
           contracts
           |> List.filter((contract: Contract.t) =>
                currentPositions
                |> Utils.orDefault([||])
                |> Js.Array.includes(contract.id)
              ),
         ),
         (
           "Past positions",
           contracts
           |> List.filter((contract: Contract.t) =>
                pastPositions
                |> Utils.orDefault([||])
                |> Js.Array.includes(contract.id)
                && !(
                     currentPositions
                     |> Utils.orDefault([||])
                     |> Js.Array.includes(contract.id)
                   )
              ),
         ),
       ];

       <>
         <Flex my={`px(12)} onClick={_ => onSelect(None)}>
           <Text
             textStyle=TextStyles.allLiquidityPositions
             textDecoration=`underline>
             {"All liquidity positions" |> React.string}
           </Text>
           <Text textStyle=TextStyles.allLiquidityPositions spaceBefore=true>
             {"(aggregate data)" |> React.string}
           </Text>
         </Flex>
         <ComplexListSearch value setValue />
         <ComplexListWrapper maxHeight=?{isMobile ? Some(`px(248)) : None}>
           {listGroups
            |> List.map(group =>
                 <ComplexListGroup
                   key={fst(group)}
                   dark=true
                   options={
                     snd(group)
                     |> List.map((contract: Contract.t) =>
                          <ComplexListOption
                            key={contract.name}
                            dark=true
                            isActive={currentContract === Some(contract)}
                            label={contract.name}
                            onClick={_ => onSelect(Some(contract))}
                          />
                        )
                   }
                   title={fst(group)}
                 />
               )
            |> Array.of_list
            |> React.array}
         </ComplexListWrapper>
       </>;
     | _ =>
       <Flex my={`px(24)} full=true justifyContent=`center>
         <Loader loaderSize=36 loaderColor=Colors.whiteLilac />
       </Flex>
     }}
  </Flex>;
};
