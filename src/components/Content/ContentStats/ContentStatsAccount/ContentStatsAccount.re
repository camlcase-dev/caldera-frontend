[@react.component]
let make = (~account: Account.t) => {
  let {isMobile, isTablet} = ResponsiveContext.useDevice();
  let {contracts, currentContractIsLoading} = ContractsContext.useContext();

  let accountLiquidity =
    CurrentData.useRawValue(
      ~config=AccountLiquidity.config(contracts, account),
      ~enabled=contracts |> Belt.Option.isSome && !currentContractIsLoading,
      (),
    );

  let accountLiquidityRewards =
    CurrentData.useRawValue(
      ~config=AccountLiquidityRewards.config(contracts, account),
      ~enabled=contracts |> Belt.Option.isSome && !currentContractIsLoading,
      (),
    );

  let currentPositions =
    accountLiquidity
    |> CurrentData.filterByValueCondition(
         (accountLiquidity: AccountLiquidity.t) =>
         accountLiquidity.lqt > 0.
       )
    |> CurrentData.getContractIds;

  let pastPositions = accountLiquidityRewards |> CurrentData.getContractIds;

  let tokenPair = Router.useTokenPair();
  let isTokenPair = tokenPair |> Belt.Option.isSome;

  <>
    <Flex flexDirection=`column px={`px(isTablet ? 24 : 20)}>
      <Flex mb={`px(16)} alignItems=`center>
        <ContentStatsAccountPositions currentPositions pastPositions />
        <ContentStatsAccountButtons />
      </Flex>
      <Flex
        className=Css.(
          style([
            selector(
              "> div",
              [
                flexGrow(1.),
                width(
                  isMobile || isTablet
                    ? `percent(isMobile ? 100. : 50.) : `initial,
                ),
                marginRight(`px(isTablet ? 0 : 16)),
                marginBottom(`px(10)),
                selector(
                  ":last-child",
                  [marginRight(`px(isTablet ? 0 : 12))],
                ),
              ],
            ),
          ])
        )
        flexWrap=`wrap
        alignItems=`flexStart
        justifyContent={isTablet ? `flexStart : `spaceBetween}
        px={`px(isTablet || !isTokenPair ? 4 : 16)}
        pb={`px(isTokenPair || !isTablet ? 14 : 10)}>
        <ContentStatsAccountLiquidity accountLiquidity />
        <ContentStatsAccountLiquidityRewards accountLiquidityRewards />
        // <ContentStatsAccountBakingRewards />
        {!isTokenPair
         |> Utils.renderIf(<ContentStatsAccountTransactions account />)}
      </Flex>
    </Flex>
    {tokenPair
     |> Utils.renderOpt(_ => <ContentStatsAccountPool accountLiquidity />)}
  </>;
};
