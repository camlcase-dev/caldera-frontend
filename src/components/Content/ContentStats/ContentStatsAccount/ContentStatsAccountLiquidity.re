[@react.component]
let make =
    (~accountLiquidity: option(array(CurrentData.t(AccountLiquidity.t)))) => {
  let {currentContract} = ContractsContext.useContext();
  let tokenPair = Router.useTokenPair();

  let (xtzPool, tokenPool) =
    accountLiquidity
    |> CurrentData.getSummedValue(
         ~contract=currentContract,
         ~sumFn=AccountLiquidity.sumFn,
       )
    |> Utils.map((accountLiquidity: AccountLiquidity.t) =>
         (Some(accountLiquidity.xtzPool), Some(accountLiquidity.tokenPool))
       )
    |> Utils.orDefault((None, None));

  let xtzPoolInCurrency =
    xtzPool |> CurrenciesContext.useValueInCurrencyString;

  let liqudityValueInCurrency =
    xtzPool
    |> Utils.map(xtzPool => xtzPool *. 2.)
    |> CurrenciesContext.useValueInCurrencyStringRounded;

  let xtzPool = Format.tokenToDisplay(xtzPool, "XTZ");

  let tokenPool =
    Format.tokenToDisplay(tokenPool, tokenPair |> TokenPair.getToken);

  <ContentStatsValue
    label="Liquidity Provided"
    tooltipText="This is the value of liquidity currently provided to Dexter (in terms of local currency)"
    value=liqudityValueInCurrency>
    {tokenPair
     |> Utils.renderOpt(_ =>
          <>
            <ContentStatsSubvalue
              currencyValue=?{
                xtzPoolInCurrency |> Utils.map(Format.addBracketsToDisplay)
              }
              value=xtzPool
            />
            <ContentStatsSubvalue
              currencyValue=?{
                xtzPoolInCurrency |> Utils.map(Format.addBracketsToDisplay)
              }
              value=tokenPool
              valueSkeletonWidth={`px(110)}
            />
          </>
        )}
  </ContentStatsValue>;
};
