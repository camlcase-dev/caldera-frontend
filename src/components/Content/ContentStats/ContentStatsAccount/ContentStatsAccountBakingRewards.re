[@react.component]
let make = () => {
  let {currentContract, currentContractIsLoading} =
    ContractsContext.useContext();

  let value =
    CurrentData.useValueInCurrencyString(
      ~config=Volume.config,
      ~contract=currentContract,
      ~cacheOnly=true,
      ~enabled=!currentContractIsLoading,
      (),
    );

  let tokenPair = Router.useTokenPair();

  <ContentStatsValue
    label="Baking Rewards"
    tooltipText="These are the lifetime baking rewards for delegation of XTZ provided as liquidity to Dexter (in terms of local currency)"
    tooltipWidth={`px(188)}
    value>
    {tokenPair
     |> Utils.renderOpt(_ =>
          <ContentStatsSubvalue
            currencyValue={"($601.14)" |> React.string}
            value={value |> Utils.map(_ => "332.921814 XTZ" |> React.string)}
          />
        )}
  </ContentStatsValue>;
};
