[@react.component]
let make =
    (~accountLiquidity: option(array(CurrentData.t(AccountLiquidity.t)))) => {
  let {currentContract} = ContractsContext.useContext();

  let (lqt, lqtPercentage) =
    accountLiquidity
    |> CurrentData.getSummedValue(
         ~contract=currentContract,
         ~sumFn=AccountLiquidity.sumFn,
       )
    |> Utils.map((accountLiquidity: AccountLiquidity.t) =>
         (Some(accountLiquidity.lqt), Some(accountLiquidity.lqtPercentage))
       )
    |> Utils.orDefault((None, None));

  let isLoading = lqt |> Belt.Option.isNone;

  <ContentStatsPool label="POOL TOKENS:" isLoading>
    <Flex ml={`px(2)} />
    <Skeleton
      isLoading
      skeletonStyle=SkeletonStyles.simpleValue
      background=Colors.cadetBlue
      width={`px(80)}>
      {lqt
       |> Utils.renderOpt(lqt =>
            <Text textStyle=TextStyles.simpleValue color=Colors.cadetBlue>
              {lqt |> Js.Float.toString |> Format.addCommas |> React.string}
            </Text>
          )}
    </Skeleton>
    <Flex ml={`px(2)} />
    <Skeleton
      isLoading skeletonStyle=SkeletonStyles.simpleValue width={`px(120)}>
      {lqtPercentage
       |> Utils.renderOpt(lqtPercentage =>
            <Text textStyle=TextStyles.simpleValue>
              {"("
               ++ (
                 lqtPercentage
                 |> Format.truncateDecimals(lqtPercentage < 1. ? 3 : 1)
               )
               ++ "% of liquidity pool)"
               |> React.string}
            </Text>
          )}
    </Skeleton>
  </ContentStatsPool>;
};
