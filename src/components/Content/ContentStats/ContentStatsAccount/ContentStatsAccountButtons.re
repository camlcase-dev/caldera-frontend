[@react.component]
let make = () => {
  let {isTablet} = ResponsiveContext.useDevice();
  let tokenPair = Router.useTokenPair();

  !isTablet
  |> Utils.renderIf(
       <Flex ml={`px(20)}>
         <LinkButton
           href={
             Utils.dexterUrl
             ++ "liquidity/add"
             ++ (tokenPair |> TokenPair.getHash)
           }
           text="ADD LIQUIDITY"
         />
         <Flex width={`px(14)} />
         <LinkButton
           href={
             Utils.dexterUrl
             ++ "liquidity/remove"
             ++ (tokenPair |> TokenPair.getHash)
           }
           text="REMOVE"
         />
       </Flex>,
     );
};
