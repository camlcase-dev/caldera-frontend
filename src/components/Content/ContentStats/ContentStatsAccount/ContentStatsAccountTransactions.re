[@react.component]
let make = (~account: string) => {
  let value = Transactions.useTransactionsNumber(~account, ());

  <ContentStatsValue label="Total transactions" value />;
};
