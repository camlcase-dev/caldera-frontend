[@react.component]
let make = () => {
  let {isMobile} = ResponsiveContext.useDevice();
  let tokenPair = Router.useTokenPair();

  <>
    <Flex
      className=Css.(
        style([
          selector(
            "> div",
            [
              marginRight(`px(isMobile ? 36 : 48)),
              marginBottom(`px(12)),
              selector(
                ":first-child",
                [width(isMobile ? `percent(100.) : `initial)],
              ),
            ],
          ),
        ])
      )
      flexWrap=`wrap
      alignItems=`flexStart
      pb={`px(isMobile ? 8 : 0)}
      px={`px(isMobile ? 24 : 28)}>
      <ContentStatsOverallLiquidity />
      <ContentStatsOverallVolume />
      {switch (tokenPair) {
       | Some(_) => <ContentStatsOverallLiquidityRewards />
       | None => <ContentStatsOverallTransactions />
       }}
    </Flex>
    {tokenPair |> Utils.renderOpt(_ => <ContentStatsOverallPool />)}
  </>;
};
