[@react.component]
let make = () => {
  let {currentContract, currentContractIsLoading} =
    ContractsContext.useContext();

  let value =
    CurrentData.useValueInCurrencyString(
      ~config=Volume.config,
      ~contract=currentContract,
      ~cacheOnly=true,
      ~enabled=!currentContractIsLoading,
      (),
    );

  <ContentStatsValue label="Volume (24hr)" value />;
};
