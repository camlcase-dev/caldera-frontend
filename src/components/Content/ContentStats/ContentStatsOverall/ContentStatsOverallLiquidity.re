[@react.component]
let make = () => {
  let {currentContract, currentContractIsLoading} =
    ContractsContext.useContext();

  let value =
    CurrentData.useValueInCurrencyString(
      ~config=Liquidity.config,
      ~contract=currentContract,
      ~cacheOnly=true,
      ~enabled=!currentContractIsLoading,
      (),
    );

  <ContentStatsValue label="Liquidity" value />;
};
