[@react.component]
let make = () => {
  let value = Transactions.useTransactionsNumber(~cacheOnly=true, ());

  <ContentStatsValue label="Transactions (24hr)" value />;
};
