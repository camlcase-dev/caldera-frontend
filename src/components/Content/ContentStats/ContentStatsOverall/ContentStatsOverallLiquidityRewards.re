[@react.component]
let make = () => {
  let {contracts, currentContract, currentContractIsLoading} =
    ContractsContext.useContext();

  let value =
    CurrentData.useValueInCurrencyString(
      ~config=LiquidityRewards.config(contracts, currentContract),
      ~contract=currentContract,
      ~enabled=!currentContractIsLoading,
      (),
    );

  <ContentStatsValue label="Rewards (24hr)" value />;
};
