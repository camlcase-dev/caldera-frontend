[@react.component]
let make = () => {
  let {currentContract} = ContractsContext.useContext();
  let value = currentContract |> Utils.map(Contract.getTokensPooled);
  let isLoading = {
    value |> Belt.Option.isNone;
  };

  <ContentStatsPool label="TOKENS POOLED" isLoading>
    <Flex className=Css.(style([opacity(0.67)])) ml={`px(6)}>
      <Skeleton
        isLoading skeletonStyle=SkeletonStyles.simpleValue width={`px(240)}>
        {value
         |> Utils.renderOpt(value =>
              <Text textStyle=TextStyles.simpleValue>
                {value |> React.string}
              </Text>
            )}
      </Skeleton>
    </Flex>
  </ContentStatsPool>;
};
