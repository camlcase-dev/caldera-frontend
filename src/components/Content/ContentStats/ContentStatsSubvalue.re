[@react.component]
let make =
    (
      ~currencyValue: option(React.element)=?,
      ~value: option(React.element),
      ~valueSkeletonWidth=`px(100),
    ) => {
  let isLoading = value |> Belt.Option.isNone;

  <Flex>
    <Skeleton
      isLoading
      skeletonStyle=SkeletonStyles.simpleValue
      background=Colors.white
      width=valueSkeletonWidth>
      <Text
        textStyle=TextStyles.simpleValue color=Colors.white fontWeight=`normal>
        {value |> Utils.renderSome}
      </Text>
    </Skeleton>
    <Flex ml={`px(2)} />
    <Skeleton
      isLoading
      skeletonStyle=SkeletonStyles.simpleValue
      background=Colors.blueBell
      width={`px(60)}>
      <Text
        textStyle=TextStyles.simpleValue
        color=Colors.blueBell
        fontWeight=`normal>
        {currencyValue |> Utils.renderSome}
      </Text>
    </Skeleton>
  </Flex>;
};
