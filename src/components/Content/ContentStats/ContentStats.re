[@react.component]
let make = () => {
  let {isMobile} = ResponsiveContext.useDevice();
  let currentRoute = Router.useCurrentRoute();

  <Box
    background={
      isMobile ? Colors.pickledBluewoodWithOpacity42 : Colors.oxfordBlue
    }
    pt={`px(20)}
    px=`zero
    pb=`zero
    mb={`px(12)}
    overflowY=`visible>
    {switch (currentRoute) {
     | Account(account, _) => <ContentStatsAccount account />
     | Overall(_) => <ContentStatsOverall />
     }}
  </Box>;
};
