[@react.component]
let make =
    (
      ~children: option(React.element)=?,
      ~label: string,
      ~tooltipText: option(string)=?,
      ~tooltipWidth: Flex.widthType=`px(170),
      ~value: option(string),
    ) => {
  let isLoading = value |> Belt.Option.isNone;

  <Flex flexDirection=`column>
    <Skeleton isLoading skeletonStyle=SkeletonStyles.mainLabel>
      <Flex>
        <Text textStyle=TextStyles.mainLabel whiteSpace=`nowrap>
          {label |> React.string}
        </Text>
        <Flex ml={`px(5)} />
        {tooltipText
         |> Utils.renderOpt(tooltipText =>
              <Tooltip
                text={
                  <Text textStyle=TextStyles.tooltip>
                    {tooltipText |> React.string}
                  </Text>
                }
                width=tooltipWidth>
                <Text fontSize={`px(11)} color=Colors.blueBell>
                  Utils.iInCircle
                </Text>
              </Tooltip>
            )}
      </Flex>
    </Skeleton>
    <Skeleton
      isLoading
      skeletonStyle=SkeletonStyles.complexStatisticValue
      width={`px(100)}>
      <Text textStyle=TextStyles.complexStatisticValue>
        {value |> Utils.renderOpt(React.string)}
      </Text>
    </Skeleton>
    {children |> Utils.renderSome}
  </Flex>;
};
