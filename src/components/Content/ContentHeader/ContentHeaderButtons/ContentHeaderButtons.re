[@react.component]
let make = () => {
  let currentRoute = Router.useCurrentRoute();

  <Flex position=`absolute right=`zero>
    {switch (currentRoute) {
     | Account(account, _) => <ContentHeaderButtonsAccount account />
     | Overall(Some(_)) => <ContentHeaderButtonsTokenPair />
     | _ => <LinkButton href=Utils.dexterUrl text="GO TO DEXTER" />
     }}
  </Flex>;
};
