[@react.component]
let make = (~account: Account.t) => {
  let {accounts, pinAccount, unpinAccount} = AccountsContext.useContext();
  let {isTablet} = ResponsiveContext.useDevice();
  let isActive = account |> Account.isPinned(accounts);

  let onClick =
    React.useCallback3(
      _ => isActive ? unpinAccount(account) : pinAccount(account),
      (isActive, account, accounts),
    );

  <Button
    isActive
    icon={<Image width={`px(15)} src="pin.svg" />}
    onClick
    text=?{isTablet ? None : Some(isActive ? "PINNED" : "PIN")}
  />;
};
