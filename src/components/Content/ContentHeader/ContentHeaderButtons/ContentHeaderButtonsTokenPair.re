[@react.component]
let make = () => {
  let {isTablet} = ResponsiveContext.useDevice();
  let tokenPair = Router.useTokenPair();

  <>
    <LinkButton
      href={Utils.dexterUrl ++ (tokenPair |> TokenPair.getHash)}
      text="EXCHANGE"
    />
    {!isTablet
     |> Utils.renderIf(
          <>
            <Flex width={`px(12)} />
            <LinkButton
              href={
                Utils.dexterUrl
                ++ "/liquidity/add"
                ++ (tokenPair |> TokenPair.getHash)
              }
              text="ADD LIQUIDITY"
            />
          </>,
        )}
  </>;
};
