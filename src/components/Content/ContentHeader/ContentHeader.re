[@react.component]
let make = () => {
  let {isMobile} = ResponsiveContext.useDevice();

  <Flex
    mx={`px(isMobile ? 8 : (-4))}
    maxWidth=`initial
    mb={`px(isMobile ? 16 : 14)}
    position=`relative
    zIndex=1>
    <ContentHeaderTitle />
    <ContentHeaderButtons />
  </Flex>;
};
