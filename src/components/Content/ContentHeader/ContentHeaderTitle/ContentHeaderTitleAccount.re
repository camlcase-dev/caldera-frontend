[@react.component]
let make = (~account: Account.t) =>
  <Flex>
    <Tooltip text={<CopyToClipboard text=account />} shift=16>
      <Text textStyle=TextStyles.mainTitle>
        {account |> Account.toDisplay |> React.string}
      </Text>
    </Tooltip>
    <Flex ml={`px(6)} mt={`px(9)} mb={`px(12)}>
      <a href={j|http://tezblock.io/account/$account|j} target="_blank">
        <Image width={`px(14)} height={`px(14)} src="external-link.svg" />
      </a>
    </Flex>
  </Flex>;
