[@react.component]
let make = (~tokenPair: string) => {
  let {currentContract} = ContractsContext.useContext();
  let {isTablet} = ResponsiveContext.useDevice();

  let (isReversed, setIsReversed) = React.useState(_ => false);

  <>
    <Text textStyle=TextStyles.mainTitle> {tokenPair |> React.string} </Text>
    <Flex width={`px(12)} />
    <Flex
      alignItems=`center
      onClick={_ => setIsReversed(_ => !isReversed)}
      borderRadius={`px(12)}
      mt={isTablet ? `px(4) : `zero}
      p={`px(4)}
      pr={`px(10)}
      background=Colors.blackWithOpacity10>
      <Image width={`px(17)} height={`px(17)} src="exchange.svg" />
      <Flex width={`px(4)} />
      <Skeleton
        isLoading={currentContract |> Belt.Option.isNone}
        skeletonStyle=SkeletonStyles.exchangeRate
        width={`px(140)}>
        <Text textStyle=TextStyles.exchangeRate>
          {switch (currentContract) {
           | Some(contract) =>
             (
               isReversed
                 ? contract |> Contract.getExchangeRateToXtz
                 : contract |> Contract.getExchangeRateFromXtz
             )
             |> React.string
           | _ => React.null
           }}
        </Text>
      </Skeleton>
    </Flex>
  </>;
};
