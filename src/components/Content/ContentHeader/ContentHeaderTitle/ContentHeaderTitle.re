[@react.component]
let make = () => {
  let {isMobile} = ResponsiveContext.useDevice();
  let currentRoute = Router.useCurrentRoute();

  <Flex alignItems=`baseline flexDirection={isMobile ? `column : `row}>
    {switch (currentRoute) {
     | Account(account, _) => <ContentHeaderTitleAccount account />
     | Overall(Some(tokenPair)) => <ContentHeaderTokenPair tokenPair />
     | _ =>
       <Text textStyle=TextStyles.mainTitle> {"Dexter" |> React.string} </Text>
     }}
  </Flex>;
};
