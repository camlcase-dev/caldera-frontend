[@react.component]
let make =
    (
      ~children: option(React.element)=?,
      ~background: option(Flex.backgroundType)=?,
      ~isLoading: bool,
      ~width: option(Flex.widthType)=?,
      ~skeletonStyle: SkeletonStyles.t,
    ) =>
  isLoading
    ? <Flex height={skeletonStyle.height} alignItems=`center>
        <Flex
          background={background |> Utils.orDefault(skeletonStyle.background)}
          className=Css.(
            style([
              opacity(0.1),
              animation(
                ~duration=3000,
                ~iterationCount=`infinite,
                keyframes([
                  (0, [opacity(0.1)]),
                  (50, [opacity(0.3)]),
                  (100, [opacity(0.1)]),
                ]),
              ),
              selector("> *", [opacity(0.), visibility(`hidden)]),
            ])
          )
          ?width
          height={skeletonStyle.skeletonHeight}
          borderRadius={`px(20)}>
          {children |> Utils.renderSome}
        </Flex>
      </Flex>
    : children |> Utils.renderSome;
