[@react.component]
let make =
    (
      ~children: React.element,
      ~background: option(Flex.backgroundType)=?,
      ~borderRadius: Css.Types.Length.t=`px(12),
      ~height: option(Flex.heightType)=?,
      ~minHeight: option(Flex.heightType)=?,
      ~overflowX: option(Css.Types.Overflow.t)=?,
      ~overflowY: Css.Types.Overflow.t=`auto,
      ~onClick: option(ReactEvent.Mouse.t => unit)=?,
      ~p: Css.Types.Length.t=`px(16),
      ~pt: option(Css.Types.Length.t)=?,
      ~px: option(Css.Types.Length.t)=?,
      ~pb: option(Css.Types.Length.t)=?,
      ~mb: option(Flex.marginType)=?,
    ) => {
  <Flex
    ?background
    ?height
    ?mb
    ?minHeight
    ?onClick
    flexDirection=`column
    flexShrink=0.
    borderRadius
    position=`relative
    p
    pt={pt |> Utils.orDefault(p)}
    px={px |> Utils.orDefault(p)}
    pb={pb |> Utils.orDefault(p)}
    ?overflowX
    overflowY>
    children
  </Flex>;
};
