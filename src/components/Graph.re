[@bs.get] external getOffsetWidth: Dom.element => int = "offsetWidth";

[@react.component]
let make =
    (
      ~crosshair: bool=false,
      ~graphSeries: option(array(Highcharts.Options.Series.t))=?,
      ~tickInterval: option(int)=?,
      ~withLegend: bool=false,
    ) => {
  let {isXXSmall} = ResponsiveContext.useDevice();
  let {currentCurrency} = CurrenciesContext.useContext();

  let graphCurrency = React.useRef(None);

  React.useEffect2(
    () => {
      switch (graphSeries, graphCurrency.current) {
      | (None, None) => graphCurrency.current = currentCurrency
      | (Some(_), _) => graphCurrency.current = currentCurrency
      | _ => ()
      };
      None;
    },
    (graphSeries, currentCurrency),
  );

  let wrapperRef = React.useRef(Js.Nullable.null);
  let (wrapperWidth, setWrapperWidth) = React.useState(_ => 0);

  let checkWidth = _ => {
    switch (Js.Nullable.toOption(wrapperRef.current)) {
    | Some(current) =>
      let newWrapperWidth = current |> getOffsetWidth;
      if (wrapperWidth !== newWrapperWidth) {
        setWrapperWidth(_ => newWrapperWidth);
      };
    | _ => ()
    };
  };

  React.useEffect1(
    () => {
      checkWidth();
      Window.addSyntheticEventListener("resize", checkWidth);
      Some(_ => Window.removeSyntheticEventListener("resize", checkWidth));
    },
    [|wrapperRef.current|],
  );

  <Flex
    elementRef={ReactDOM.Ref.domRef(wrapperRef)}
    className=Css.(
      style([
        selector(
          ".highcharts-tooltip",
          [
            background(Colors.whiteLilac),
            padding(`px(14)),
            borderRadius(`px(8)),
            overflow(`hidden),
            selector(
              "span",
              [
                display(`flex),
                flexDirection(`column),
                important(position(`static)),
                important(fontSize(`px(10))),
                letterSpacing(`px(1)),
                color(Colors.tundora),
                selector(
                  "strong",
                  [
                    fontWeight(`bold),
                    color(Colors.tundora),
                    important(fontSize(`px(11))),
                    important(letterSpacing(`pxFloat(0.28))),
                    lineHeight(`px(15)),
                    marginBottom(`px(2)),
                  ],
                ),
              ],
            ),
          ],
        ),
      ])
    )
    height={`px(withLegend ? 304 : 268)}
    width={`percent(100.)}
    mb={`px(withLegend ? 26 : 30)}>
    {switch (graphSeries) {
     | Some(graphSeries) when wrapperWidth > 0 =>
       <HighchartsReact
         options=Highcharts.Options.(
           make(
             ~chart=
               Chart.make(
                 ~width=wrapperWidth,
                 ~marginTop=40,
                 ~spacing=(0, 0, 0, 0),
                 ~style=CssObject.make(~fontFamily="Source Sans Pro", ()),
                 (),
               ),
             ~series=graphSeries,
             ~title=Title.make(~text=None, ()),
             ~tooltip=Highcharts.Tooltip.make(graphCurrency.current),
             ~legend=Highcharts.Legend.make(withLegend, isXXSmall ? 16 : 24),
             ~xAxis=
               Axis.make(
                 ~_type=`datetime,
                 ~gridLineWidth=0,
                 ~labels=
                   AxisLabel.make(
                     ~style=
                       CssObject.make(
                         ~color="#" ++ Colors.whiteLilacHex,
                         ~fontSize="13px",
                         ~fontWeight="600",
                         ~letterSpacing="0.5px",
                         (),
                       ),
                     (),
                   ),
                 ~lineWidth=0,
                 ~tickLength=0,
                 ~tickInterval?,
                 ~title=Title.make(~text=None, ()),
                 ~crosshair=?{
                   crosshair
                     ? Some(
                         Crosshair.make(
                           ~color="#" ++ Colors.whiteLilacHex,
                           ~dashStyle="solid",
                           (),
                         ),
                       )
                     : None;
                 },
                 (),
               ),
             ~yAxis=
               Axis.make(
                 ~gridLineWidth=0,
                 ~labels=
                   AxisLabel.make(
                     ~formatter=
                       [@bs.this] this => {
                         let value =
                           Numeral.format(
                             Numeral.make(`Float(this##value)),
                             ~format="0a",
                             (),
                           )
                           |> Js.String.toUpperCase;

                         this##isFirst
                           ? ""
                           : graphCurrency.current
                             |> Utils.map(currency =>
                                  Currency.addCurrencySymbol(currency, value)
                                )
                             |> Utils.orDefault(value);
                       },
                     ~style=
                       CssObject.make(
                         ~color="#" ++ Colors.whiteLilacHex,
                         ~fontSize="13px",
                         ~fontWeight="600",
                         ~letterSpacing="0.5px",
                         (),
                       ),
                     (),
                   ),
                 ~tickAmount=3,
                 ~lineWidth=0,
                 ~tickLength=0,
                 ~title=Title.make(~text=None, ()),
                 (),
               ),
             ~credits=Credits.make(~enabled=false, ()),
             (),
           )
         )
       />
     | _ =>
       <Flex alignItems=`center justifyContent=`center flexGrow=1.>
         <Loader loaderColor=Colors.malachite />
       </Flex>
     }}
  </Flex>;
};
