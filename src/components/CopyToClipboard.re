let copyToClipboard: Dom.element => unit = [%bs.raw
  {|
     function(input) {
       if (input) {
        input.select();
        document.execCommand("copy");
       }
     }
    |}
];

[@react.component]
let make = (~text: string) => {
  let {showMessage} = MessageContext.useContext();
  let inputRef = React.useRef(Js.Nullable.null);

  <Flex
    position=`relative
    pr={`px(15)}
    onClick={_ =>
      Js.Nullable.toOption(inputRef.current)
      |> Utils.map(input => {
           copyToClipboard(input);
           showMessage("Copied to clipboard");
         })
      |> Utils.orDefault()
    }>
    <input
      className=Css.(style([opacity(0.), position(`absolute), zIndex(-1)]))
      ref={ReactDOM.Ref.domRef(inputRef)}
      defaultValue=text
    />
    <Text textStyle=TextStyles.tooltip whiteSpace=`nowrap>
      {text |> React.string}
    </Text>
    <Image
      className=Css.(
        style([
          position(`absolute),
          right(`zero),
          top(`percent(50.)),
          transform(translateY(`percent(-50.))),
        ])
      )
      width={`px(12)}
      height={`px(12)}
      src="copy.svg"
    />
  </Flex>;
};
