open Css;

global("body", [margin(`zero)]);
global("button", [padding(`zero)]);
global("html", [background(Colors.oxfordBlue)]);
global("input", [outline(`zero, `solid, Colors.white)]);
global("*", [fontFamily(`custom("Source Sans Pro"))]);
