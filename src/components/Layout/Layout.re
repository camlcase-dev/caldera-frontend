include Layout_Styles;

[@react.component]
let make = () => {
  let {isMobile}: ResponsiveContext.Device.t = ResponsiveContext.useDevice();

  <Flex flexDirection=`column alignItems=`center height={`vh(100.)}>
    <Navbar />
    <Flex
      maxWidth={`px(1024)}
      width={`percent(100.)}
      py={`px(12)}
      px={`px(isMobile ? 12 : 20)}
      flexShrink=0.
      flexDirection={isMobile ? `column : `row}>
      <Flex
        width={isMobile ? `percent(100.) : `px(228)}
        flexDirection=`column
        flexShrink=0.>
        <Menu />
      </Flex>
      {!isMobile |> Utils.renderIf(<Flex width={`px(12)} flexShrink=0. />)}
      <Flex
        width={
          isMobile
            ? `percent(100.) : `calc((`sub, `percent(100.), `px(268)))
        }
        flexDirection=`column
        flexGrow=1.>
        <Content />
      </Flex>
    </Flex>
  </Flex>;
};
