[@react.component]
let make = () => {
  <>
    <Flex mb={`px(4)} />
    <Text textStyle=TextStyles.currenciesDropdownError>
      {"Pricing data is currently unavailable." |> React.string}
    </Text>
    <Text textStyle=TextStyles.currenciesDropdownError>
      {"Please check back again soon." |> React.string}
    </Text>
    <Flex mb={`px(24)} />
  </>;
};
