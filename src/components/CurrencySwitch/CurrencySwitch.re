[@react.component]
let make = () => {
  let {currentCurrency, currenciesError}: CurrenciesContext.t =
    CurrenciesContext.useContext();
  let (listVisible, setListVisible) = React.useState(_ => false);

  let toggleListVisible =
    React.useCallback0(_ => setListVisible(listVisible => !listVisible));

  <Dropdown
    button={
      <DropdownButton
        background=Colors.whiteLilac
        color=?{currenciesError ? Some(Colors.mandy) : None}
        onClick=toggleListVisible
        textDecoration=?{currenciesError ? Some(`lineThrough) : None}
        width={`px(55)}>
        <Flex width={`px(22)}>
          {currentCurrency
           |> Utils.map((currency: Currency.t) => currency.symbol)
           |> Utils.orDefault(LocalStorage.Currency.get())
           |> Js.String.toUpperCase
           |> React.string}
        </Flex>
      </DropdownButton>
    }
    bottom={`px(-8)}
    list={<CurrencySwitchList toggleListVisible />}
    listVisible
    toggleListVisible
  />;
};
