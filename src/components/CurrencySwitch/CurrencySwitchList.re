[@react.component]
let make = (~toggleListVisible: unit => unit) => {
  let {currentCurrency, setCurrentCurrency, currencies, currenciesError}: CurrenciesContext.t =
    CurrenciesContext.useContext();

  let (value, setValue) = React.useState(_ => "");

  let onSelect =
    React.useCallback0(currency => {
      setCurrentCurrency(currency);
      toggleListVisible();
    });

  <Flex
    background=Colors.whiteLilac
    px={`px(16)}
    pt={`px(24)}
    width={`px(276)}
    maxHeight={`px(402)}
    maxWidth={`calc((`sub, `vw(100.), `px(48)))}
    flexDirection=`column
    borderRadius={`px(16)}
    alignItems=`center>
    <Text textStyle=TextStyles.currenciesDropdownTitle>
      {"Select local currency" |> React.string}
    </Text>
    {switch (currenciesError) {
     | true => <CurrencySwitchError />
     | false when currencies |> List.length === 0 =>
       <Flex mt={`px(24)} mb={`px(36)}> <Loader loaderSize=36 /> </Flex>
     | _ =>
       let currencies = currencies |> Currency.filterByText(value);
       let listGroups = [
         ("Popular currencies", currencies |> Currency.getPopular),
         ("Other currencies (A-Z)", currencies |> Currency.getOther),
       ];

       <>
         <Flex mb={`px(8)} />
         <ComplexListSearch value setValue />
         <ComplexListWrapper>
           {listGroups
            |> List.map(group =>
                 <ComplexListGroup
                   key={fst(group)}
                   options={
                     snd(group)
                     |> List.map(currency =>
                          <ComplexListOption
                            key={currency.symbol}
                            isActive={currentCurrency === Some(currency)}
                            symbol={currency.symbol |> Js.String.toUpperCase}
                            label={currency.name}
                            onClick={_ => onSelect(currency)}
                          />
                        )
                   }
                   title={fst(group)}
                 />
               )
            |> Array.of_list
            |> React.array}
         </ComplexListWrapper>
       </>;
     }}
  </Flex>;
};
