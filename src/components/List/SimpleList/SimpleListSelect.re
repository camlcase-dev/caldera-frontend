[@react.component]
let make =
    (
      ~onBlur: _ => unit,
      ~onOptionClick: string => unit,
      ~options: list(SimpleList.t),
      ~value: string,
    ) => {
  <select
    className=Css.(
      style([
        opacity(0.),
        width(pct(100.)),
        height(pct(100.)),
        position(`absolute),
      ])
    )
    onChange={ev => onOptionClick(ev |> Utils.eventToValue)}
    onBlur
    value>
    {options
     |> List.map(({label, value}: SimpleList.t) => {
          <option key=value value> {label |> React.string} </option>
        })
     |> Array.of_list
     |> React.array}
  </select>;
};
