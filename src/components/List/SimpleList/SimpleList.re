type t = {
  label: string,
  value: string,
};

[@react.component]
let make =
    (
      ~onOptionClick: string => unit,
      ~options: list(t),
      ~px: Css.Types.Length.t=`px(22),
      ~width: Flex.widthType=`px(112),
      ~value: string,
    ) =>
  <Flex
    flexDirection=`column
    borderRadius={`px(14)}
    background=Colors.whiteLilac
    width
    px
    pt={`px(16)}
    pb={`px(4)}>
    {options
     |> List.map((option: t) => {
          let isCurrent = option.value === value;

          <Flex
            key={option.value}
            mb={`px(12)}
            onClick=?{
              isCurrent ? None : Some(_ => onOptionClick(option.value))
            }>
            <Text
              full=true
              fontWeight={isCurrent ? `bold : `normal}
              textStyle=TextStyles.dropdownButtonText
              color={isCurrent ? Colors.malachite : Colors.tundora}
              hoverColor=Colors.malachite
              textAlign=`right>
              {option.label |> React.string}
            </Text>
          </Flex>;
        })
     |> Array.of_list
     |> React.array}
  </Flex>;
