[@react.component]
let make = (~dark: bool=false, ~options: list(React.element), ~title: string) => {
  options
  |> List.length > 0
  |> Utils.renderIf(
       <>
         <Flex ml={`px(5)} mb={`px(5)} mt={`px(2)} flexShrink=0.>
           <Text
             color={dark ? Colors.white : Colors.fiord}
             textStyle=TextStyles.listGroupTitle>
             {title |> React.string}
           </Text>
         </Flex>
         {options |> Array.of_list |> React.array}
       </>,
     );
};
