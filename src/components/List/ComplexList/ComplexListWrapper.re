[@react.component]
let make =
    (~children: React.element, ~maxHeight: Flex.maxHeightType=`px(368)) => {
  <Flex
    className=Css.(
      style([borderTopLeftRadius(`px(16)), borderTopRightRadius(`px(16))])
    )
    flexDirection=`column
    full=true
    maxHeight
    mt={`px(8)}
    overflowY=`auto
    pb={`px(12)}>
    children
  </Flex>;
};
