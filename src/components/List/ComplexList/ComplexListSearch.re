[@react.component]
let make = (~value: string, ~setValue: (string => string) => unit) => {
  let onChange =
    React.useCallback0(ev => {
      let value = ev |> Utils.eventToValue;
      setValue(_ => value);
    });

  <input
    autoFocus=true
    placeholder="Search..."
    className=Css.(
      style([
        background(Colors.white),
        border(`zero, `none, Colors.white),
        fontSize(px(10)),
        padding2(~v=`zero, ~h=`px(15)),
        boxSizing(`borderBox),
        borderRadius(`px(16)),
        height(`px(32)),
        color(Colors.black),
        Css.placeholder([color(Colors.jumbo)]),
        Css.width(pct(100.)),
        flexShrink(0.),
      ])
    )
    value
    onChange
  />;
};
