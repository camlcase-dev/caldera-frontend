[@react.component]
let make =
    (
      ~dark: bool=false,
      ~isActive: bool,
      ~symbol: option(string)=?,
      ~label: string,
      ~onClick: _ => unit,
    ) =>
  <Flex
    alignItems=`center
    background={dark ? Colors.oxfordBlue : Colors.white}
    borderRadius={`px(4)}
    flexShrink=0.
    full=true
    height={`px(54)}
    justifyContent=`spaceBetween
    mb={`px(8)}
    px={`px(18)}
    onClick>
    <Flex alignItems=`center>
      {symbol
       |> Utils.renderOpt(symbol => {
            <>
              <Text
                textStyle=TextStyles.listOptionSymbol
                color={dark ? Colors.white : Colors.black}>
                {symbol |> React.string}
              </Text>
              <Flex mr={`px(6)} />
            </>
          })}
      <Text
        textStyle=TextStyles.listOptionLabel
        color={dark ? Colors.whiteLilac : Colors.tundora}>
        {label |> React.string}
      </Text>
    </Flex>
    {isActive
     |> Utils.renderIf(
          <Image
            className=?{
              dark
                ? Some(
                    Css.(
                      style([
                        filter([`brightness(1000.), `grayscale(100.)]),
                      ])
                    ),
                  )
                : None
            }
            width={`px(16)}
            src={dark ? "checkmark.svg" : "checkmark-d.svg"}
          />,
        )}
  </Flex>;
