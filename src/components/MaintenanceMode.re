[@react.component]
let make = () => {
  <Flex flexDirection=`column alignItems=`center height={`vh(100.)}>
    <Navbar />
    <Flex m={`px(12)}>
      <Box background={Css.hex("3E4d67")} p={`px(32)}>
        <Flex mb={`px(8)} flexDirection=`column alignItems=`center>
          <Flex flexDirection=`column mb={`px(20)}>
            <Text
              textStyle=Css.[letterSpacing(pxFloat(1.1))]
              fontSize={`px(18)}
              lineHeight={`px(24)}
              fontWeight=`semiBold
              color=Colors.white>
              {"Dexter Stats is currently down for maintenance."
               |> React.string}
            </Text>
            <Flex mb={`px(4)} />
            <Text
              textStyle=Css.[letterSpacing(pxFloat(1.1))]
              fontSize={`px(15)}
              color=Colors.white>
              {"We hope to be back online soon." |> React.string}
            </Text>
          </Flex>
          <Image
            className=Css.(style([width(`px(240)), marginLeft(`px(20))]))
            src="tezilla.png"
          />
        </Flex>
      </Box>
    </Flex>
  </Flex>;
};
