type listPosition =
  | Top
  | Bottom;

[@react.component]
let make =
    (
      ~button: React.element,
      ~bottom: option(Flex.positionLenghtsType)=?,
      ~background: Flex.backgroundType=`transparent,
      ~right: Flex.positionLenghtsType=`zero,
      ~top: option(Flex.positionLenghtsType)=?,
      ~list: React.element,
      ~listVisible: bool,
      ~listPosition: listPosition=Bottom,
      ~toggleListVisible: _ => unit,
    ) =>
  <Flex position=`relative flexGrow=1.>
    button
    {listVisible
     |> Utils.renderIf(
          <>
            <Flex
              className=Css.(
                style([
                  transform(
                    translateY(
                      `percent(listPosition === Bottom ? 100. : (-100.)),
                    ),
                  ),
                ])
              )
              zIndex=3
              position=`absolute
              ?top
              ?bottom
              right
              maxWidth=`initial
              minWidth={`percent(100.)}>
              list
            </Flex>
            <Flex
              background
              bottom=`zero
              left=`zero
              onClick=toggleListVisible
              position=`fixed
              right=`zero
              top=`zero
              width={`percent(100.)}
              zIndex=2
            />
          </>,
        )}
  </Flex>;
