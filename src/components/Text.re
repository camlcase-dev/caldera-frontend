open Utils;

type fontWeightType = [ Css.Types.FontWeight.t | Css.Types.Cascading.t];
type fontSizeType = [ Css.Types.Length.t | Css.Types.Cascading.t];
type textAlignType = [ Css.Types.TextAlign.t | Css.Types.Cascading.t];
type textFontStyleType = [ Css.Types.FontStyle.t | Css.Types.Cascading.t];
type textLineHeightType = [
  Css.Types.LineHeight.t
  | Css.Types.Length.t
  | Css.Types.Cascading.t
];
type letterSpacingType = [
  Css.Types.LetterSpacing.t
  | Css.Types.Length.t
  | Css.Types.Var.t
  | Css.Types.Cascading.t
];
type textTransformType = [ Css.Types.TextTransform.t | Css.Types.Cascading.t];
type whiteSpaceType = [ Css.Types.WhiteSpace.t | Css.Types.Cascading.t];
type textDecorationType = [
  | `none
  | `underline
  | `overline
  | `lineThrough
  | Css.Types.Var.t
  | Css.Types.Cascading.t
];

[@react.component]
let make =
    (
      ~children: React.element,
      ~className: string="",
      ~color: option(Css.Types.Color.t)=?,
      ~cursor: option(Css.Types.Cursor.t)=?,
      ~ellipsis=false,
      ~fontFamily: string="Source Sans Pro",
      ~fontSize: option(fontSizeType)=?,
      ~fontStyle: option(textFontStyleType)=?,
      ~fontWeight: option(fontWeightType)=?,
      ~full: bool=false,
      ~hoverColor: option(Css.Types.Color.t)=?,
      ~hoverFontWeight: option(fontWeightType)=?,
      ~inlineBlock: bool=true,
      ~letterSpacing: option(letterSpacingType)=?,
      ~lineHeight: option(textLineHeightType)=?,
      ~spaceAfter: bool=false,
      ~spaceBefore: bool=false,
      ~textAlign: option(textAlignType)=?,
      ~textDecoration: option(textDecorationType)=?,
      ~textStyle: option(TextStyles.t)=?,
      ~textTransform: option(textTransformType)=?,
      ~whiteSpace: option(whiteSpaceType)=?,
    ) => {
  let hoverStyle =
    switch (hoverColor, hoverFontWeight) {
    | (Some(hoverColor), Some(hoverFontWeight)) =>
      Some([
        Css.selector("> *", [Css.color(hoverColor)]),
        Css.color(hoverColor),
        Css.fontWeight(hoverFontWeight),
      ])
    | (Some(hoverColor), _) =>
      Some([
        Css.selector("> *", [Css.color(hoverColor)]),
        Css.color(hoverColor),
      ])
    | (_, Some(hoverFontWeight)) => Some([Css.fontWeight(hoverFontWeight)])
    | _ => None
    };

  let style =
    [
      color |> map(color => Css.color(color)),
      color |> map(color => Css.selector("> *", [Css.color(color)])),
      fontStyle |> map(fontStyle => Css.fontStyle(fontStyle)),
      fontWeight |> map(fontWeight => Css.fontWeight(fontWeight)),
      textAlign |> map(textAlign => Css.textAlign(textAlign)),
      letterSpacing |> map(letterSpacing => Css.letterSpacing(letterSpacing)),
      lineHeight |> map(lineHeight => Css.lineHeight(lineHeight)),
      fontSize |> map(fontSize => Css.fontSize(fontSize)),
      textDecoration
      |> map(textDecoration => Css.textDecoration(textDecoration)),
      textTransform |> map(textTransform => Css.textTransform(textTransform)),
      hoverStyle |> map(hoverStyle => Css.hover(hoverStyle)),
      whiteSpace |> map(whiteSpace => Css.whiteSpace(whiteSpace)),
      cursor |> map(cursor => Css.cursor(cursor)),
    ]
    |> filterNone;

  let elementClassName =
    Css.merge([
      "text",
      Css.style(textStyle |> orDefault([])),
      Css.style([
        Css.display(inlineBlock ? `inlineBlock : `inline),
        Css.fontFamilies([`custom(fontFamily), `sansSerif]),
        Css.width(full ? Css.pct(100.) : `auto),
        Css.textOverflow(ellipsis ? `ellipsis : `initial),
        Css.maxWidth(Css.pct(100.)),
        Css.overflow(ellipsis ? `hidden : `visible),
        Css.before(spaceBefore ? [Css.contentRule(`text("\\00a0 "))] : []),
        Css.after(spaceAfter ? [Css.contentRule(`text("\\00a0 "))] : []),
        ...style,
      ]),
      className,
    ]);

  <span className=elementClassName> children </span>;
};
