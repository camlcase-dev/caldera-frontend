[@react.component]
let make =
    (
      ~className: option(string)=?,
      ~height: option(Flex.heightType)=?,
      ~width: option(Flex.widthType)=?,
      ~rotate: option(float)=?,
      ~src: string,
    ) => {
  let className =
    Css.merge([
      Css.style(
        [
          height |> Utils.map(height => Css.height(height)),
          width |> Utils.map(width => Css.width(width)),
          rotate
          |> Utils.map(rotate => Css.transform(Css.rotate(`deg(rotate)))),
        ]
        |> Utils.filterNone,
      ),
      className |> Utils.orDefault(""),
    ]);

  <img className src={"/img/" ++ src} />;
};
