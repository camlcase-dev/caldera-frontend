[@react.component]
let make =
    (
      ~label: string,
      ~value: option(string),
      ~valueSkeletonWidth: option(Flex.widthType)=?,
    ) => {
  let {isMobile} = ResponsiveContext.useDevice();
  let isLoading = value |> Belt.Option.isNone;

  <Flex ml={`px(4)} mr={`px(28)} flexDirection={isMobile ? `column : `row}>
    <Skeleton isLoading skeletonStyle=SkeletonStyles.simpleValueLabel>
      <Text textStyle=TextStyles.simpleValueLabel>
        {label |> React.string}
      </Text>
    </Skeleton>
    <Flex width={`px(6)} />
    <Skeleton
      isLoading
      skeletonStyle=SkeletonStyles.simpleValue
      width=?valueSkeletonWidth>
      {value
       |> Utils.renderOpt(value =>
            <Text textStyle=TextStyles.simpleValue>
              {value |> React.string}
            </Text>
          )}
    </Skeleton>
  </Flex>;
};
