[@react.component]
let make = () => {
  let {isMobile, isTablet} = ResponsiveContext.useDevice();

  let liquidityValue =
    CurrentData.useValueInCurrencyString(~config=Liquidity.config, ());

  let volumeValue =
    CurrentData.useValueInCurrencyString(~config=Volume.config, ());

  let transactionsNumber = Transactions.useTransactionsNumber();

  <Flex
    alignItems=`center
    height={`px(56)}
    width={`percent(100.)}
    justifyContent=`center
    background=Colors.pickledBluewood
    flexShrink=0.>
    <Flex
      maxWidth={`px(1024)}
      width={`percent(100.)}
      px={`px(isMobile ? 24 : 28)}
      justifyContent=`spaceBetween
      alignItems=`center>
      <a
        className=Css.(style([textDecoration(`none), whiteSpace(`nowrap)]))
        href="/">
        <Flex flexWrap=`nowrap>
          <Image
            className=Css.(style([position(`relative), top(`px(3))]))
            width={`px(115)}
            src="logo.svg"
          />
          <Flex ml={`px(14)} mt={`px(1)}>
            <Text textStyle=TextStyles.logoText>
              {"STATS" |> React.string}
            </Text>
          </Flex>
        </Flex>
      </a>
      {!Utils.isMaintenanceMode()
       |> Utils.renderIf(
            <Flex alignItems=`center>
              {!isTablet
               |> Utils.renderIf(
                    <>
                      <NavbarValue
                        label="TOTAL LIQUIDITY"
                        value=liquidityValue
                        valueSkeletonWidth={`px(60)}
                      />
                      <NavbarValue
                        label="VOL (24HR)"
                        value=volumeValue
                        valueSkeletonWidth={`px(60)}
                      />
                      <NavbarValue
                        label="TXNS (24HR)"
                        value=transactionsNumber
                        valueSkeletonWidth={`px(30)}
                      />
                    </>,
                  )}
              <CurrencySwitch />
            </Flex>,
          )}
    </Flex>
  </Flex>;
};
