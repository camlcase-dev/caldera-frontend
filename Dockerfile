FROM tiangolo/node-frontend:10 as build-stage

ARG api_user
ARG api_password
ARG prod
ARG staging
ARG maintenance_mode
ARG amplitude_api_key
ARG sentry_dsn
ARG env

ENV CALDERA_FRONTEND_PROD=$prod
ENV CALDERA_FRONTEND_STAG=$staging
ENV CALDERA_FRONTEND_MAINTENANCE_MODE=$maintenance_mode
ENV CALDERA_API_USERNAME=$api_user
ENV CALDERA_API_PASSWORD=$api_password
ENV AMPLITUDE_API_KEY=$amplitude_api_key
ENV SENTRY_DSN=$sentry_dsn
ENV NODE_ENV=$env

WORKDIR /app
COPY package*.json /app/
RUN npm install
COPY ./ /app/
RUN yarn build

FROM nginx:latest
COPY nginx.server.conf /etc/nginx/conf.d/nginx.server.conf
COPY --from=build-stage /app/build/ /usr/share/nginx/html