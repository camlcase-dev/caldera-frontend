const path = require('path')
const webpack = require('webpack')
const outputDir = path.join(__dirname, 'build/')

const isProd = process.env.NODE_ENV === 'production'

module.exports = {
  node: { global: true, fs: 'empty' }, // Fix: "Uncaught ReferenceError: global is not defined", and "Can't resolve 'fs'".
  entry: './lib/js/src/Index.js',
  mode: isProd ? 'production' : 'development',
  output: {
    path: outputDir,
    publicPath: '',
    filename: 'bundle.js'
  },
  plugins: [
    new webpack.DefinePlugin({
      VERSION: JSON.stringify(require('./package.json').version),
      PROD: JSON.stringify(process.env.CALDERA_FRONTEND_PROD || false),
      STAG: JSON.stringify(process.env.CALDERA_FRONTEND_STAG || false),
      MAINTENANCE_MODE: JSON.stringify(
        process.env.CALDERA_FRONTEND_MAINTENANCE_MODE
      ),
      AUTH_BASIC: JSON.stringify(
        Buffer.from(
          process.env.CALDERA_API_USERNAME +
            ':' +
            process.env.CALDERA_API_PASSWORD
        ).toString('base64')
      ),
      AMPLITUDE_API_KEY: JSON.stringify(process.env.AMPLITUDE_API_KEY),
      SENTRY_DSN: JSON.stringify(process.env.SENTRY_DSN)
    })
  ],
  devServer: {
    compress: true,
    contentBase: outputDir,
    port: process.env.PORT || 8011,
    historyApiFallback: true
  },
  module: {
    rules: [
      {
        test: /\.html$/,
        use: [
          {
            loader: 'html-loader',
            options: {
              attrs: ['img:src']
            }
          }
        ]
      },
      {
        test: /\.(png|jpg|gif|svg|css)$/,
        use: 'url-loader'
      }
    ]
  }
}
