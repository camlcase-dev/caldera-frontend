module.exports = {
  testMatch: ['**/__tests__/**/*_Spec.js'],
  collectCoverage: true,
  collectCoverageFrom: ['lib/js/src/**/!(*_Style).js'],
  snapshotResolver: '<rootDir>/snapshotResolver.js',
  snapshotSerializers: ['@emotion/jest/serializer'],
  globals: {
    PROD: false,
    STAG: false,
    VERSION: '1.2.34',
    MAINTENANCE_MODE: false,
    SENTRY_DSN: ''
  }
}
